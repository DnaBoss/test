const GameBase = require('../../../utils/base/gameBase.js');
const consts = require('../../../config/consts.json');

class PrimtiveGame extends GameBase {
    constructor(tableId, setting) {
        super(tableId, setting);
        // TODO:add game property
    }

    init() {
        // TODO:init dummy pool,real pool,dummy bet time ,real bet time
    }

    makeFlow(factor) {
        // TODO: check time
        // TODO: produce dummy pool , dummy person , real pooo, real person
    }

    async countBet(player, data) {
        // TODO: CALL this.api.getTicket 
        // TODO: Accumulate
    }

    settle() {
        // TODO: adjustment settle result by offset when winloss >0
        // TODO: sendToAll result 
        // TODO: keep dieHistory length < 10
        // TODO: push current result to dieHistory
    }


    sendGameInfo() {
        const data = {

        }
        this.sendToFocus('gameInfo', data);
        return data;
    }

    sendBetRes(player, data) {
        const betResData = {};
        this.sendToClient(data.uid, 'betRes', betResData);
        return betResData;
    }

    sendBetErr(player, data, err) {
        this.pendingBets[this.roundId]--;
        let resData;
        if (err.errCode) {
            resData = {};
        }
        else {
            resData = {};
        }
        this.sendToClient(data.uid, 'betRes', resData);
        return resData;
    }

    sendStateChange(currentTime) {
        let stateChangeData = {};
        this.sendToAll("stateChange", stateChangeData);
        return stateChangeData;
    }

    sendRebuildInfo(player, data) {
        let rebuildData = {};
        this.sendToClient(data.uid, "rebuildInfo", rebuildData);
        return rebuildData;
    }

}

module.exports = PrimtiveGame;