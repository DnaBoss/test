const GameBase = require('../../../utils/base/gameBase.js');
const { getRandomInt } = require('../../../utils/utility');
const consts = require('../../../config/consts.json');
class BigSmallGame extends GameBase {
    constructor(tableId, setting) {
        super(tableId, setting);
        this.bigPool = 0;
        this.bigActualPool = 0;
        this.bigNumber = 0;
        this.smallPool = 0;
        this.smallActualPool = 0;
        this.smallNumber = 0;
    }

    /**
     *
     *
     * @memberof BigSmallGame
     */
    init() {
        this.bigPool = 0;
        this.bigActualPool = 0;
        this.bigNumber = 0;
        this.smallPool = 0;
        this.smallActualPool = 0;
        this.smallNumber = 0;
    }

    redisInit() {
        let initData = {
            bigPool: 0,
            bigNumber: 0,
            smallPool: 0,
            smallNumber: 0,
        }
        this.redisDao.clearGameBet(this.tableId, initData);
    }

    /**
     *
     *
     * @param {*} factor
     * @returns
     * @memberof BigSmallGame
     */
    async makeFlow(factor) {
        if (this.nextStateTime - Date.now() > this.betTime - 3000) {
            return;
        }
        let redisGameBet = await this.redisDao.getGameBet(this.tableId);
        redisGameBet = redisGameBet || {
            bigNumber: this.bigNumber, bigPool: this.bigPool, smallNumber: this.smallNumber, smallPool: this.smallPool
        };

        if (factor == 1) {
            let bigFlowNumber = Math.floor(Math.random() * (this.persons[1] - this.persons[0])) + this.persons[0];
            let bigFlowPool = bigFlowNumber * (Math.floor(Math.random() * (this.betAmount[1] - this.betAmount[0])) + this.betAmount[0]);
            this.bigNumber = ~~redisGameBet.bigNumber + bigFlowNumber;
            this.bigPool = ~~redisGameBet.bigPool + (bigFlowPool - bigFlowPool % this.unit);
        }
        if (factor == 0) {
            let smallFlowNumber = Math.floor(Math.random() * (this.persons[1] - this.persons[0])) + this.persons[0];
            let smallFlowPool = smallFlowNumber * (Math.floor(Math.random() * (this.betAmount[1] - this.betAmount[0])) + this.betAmount[0]);
            this.smallNumber = ~~redisGameBet.smallNumber + smallFlowNumber;
            this.smallPool = ~~redisGameBet.smallPool + (smallFlowPool - smallFlowPool % this.unit);
        }
    }

    /**
     *
     *
     * @param {*} player
     * @param {*} data
     * @memberof BigSmallGame
     */
    async countBet(player, data) {
        await this.api.getTicket(player.uid, player.actualUcid, this.tableId, this.roundId, data.amount, data.choice == "big" ? 0 : 1, player.ip, player.serialNumber, data.lang);
        this.pendingBets[this.roundId]--;
        if (data.choice == "big") {
            this.bigPool += data.amount;
            this.bigActualPool += data.amount;
            this.bigNumber += 1;
        }
        else {
            this.smallPool += data.amount;
            this.smallActualPool += data.amount;
            this.smallNumber += 1;
        }
    }

    /**
     *
     *
     * @param {*} choice
     * @returns
     * @memberof BigSmallGame
     */
    checkBetChoice(choice) {
        let data = { code: "00-004", msg: "illegal choice" }
        let ok = true;
        if (choice != "big" && choice != "small") {
            ok = false;
        }
        return { ok, data };
    }

    /**
     *
     *
     * @param {*} winloss
     * @param {*} offset
     * @returns
     * @memberof BigSmallGame
     */
     adjustThreshold(winloss, offset, redisGameBet) {

        let threshold = 127;
        redisGameBet = redisGameBet || {
            bigPool: this.bigActualPool, smallPool: this.smallActualPool
        };
        this.bigActualPool = ~~redisGameBet.bigPool || this.bigActualPool;
        this.smallActualPool = ~~redisGameBet.smallPool || this.smallActualPool;
        if (winloss > 0) {
            if (this.bigActualPool > this.smallActualPool) {
                threshold = 127 + offset;
            }
            if (this.bigActualPool < this.smallActualPool) {
                threshold = 127 - offset;
            }
        }



        return threshold;


    }

    /**
     *
     *
     * @returns
     * @memberof BigSmallGame
     */
    async  settle() {
        let redisGameBet = await this.redisDao.getGameBet(this.tableId);
        let threshold = this.adjustThreshold(this.winloss, this.offset, redisGameBet);
        let randomInt = getRandomInt();
        if (randomInt <= threshold) {
            this.dieNumber = Math.ceil(Math.random() * 3);
        }
        else {
            this.dieNumber = Math.ceil(Math.random() * 3) + 3;
        }
        //this.dieNumber = Math.ceil(Math.random() * 6);

        let resultData = {
            rid: this.roundId,
            dieNumber: this.dieNumber,
            tid: this.tableId
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.RESULT, data: resultData })
        if (this.dieHistory.length == 10) {
            this.dieHistory.shift();
        }
        this.dieHistory.push({
            rid: this.roundId,
            dieNumber: this.dieNumber
        });

        return {
            winBT: this.dieNumber <= 3 ? 1 : 0,
            balls: [this.dieNumber]
        }
    }

    /**
     *
     *
     * @returns {{rid:number,currentTime:number,pool:any,number:any}}
     * @memberof BigSmallGame
     */
    sendGameInfo() {
        const gameInfoData = {
            rid: this.roundId,
            currentTime: Date.now(),
            pool: {
                big: this.bigPool,
                small: this.smallPool
            },
            number: {
                big: this.bigNumber,
                small: this.smallNumber
            },
            tid: this.tableId
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.GAME_INFO, data: gameInfoData });
        return gameInfoData;
    }

    /**
     *
     *
     * @param {*} player
     * @param {*} data
     * @returns
     * @memberof BigSmallGame
     */
    sendBetRes(player, data) {
        const betResData = {
            rid: this.roundId,
            code: "00-000",
            msg: "success",
            pool: {
                big: this.bigPool,
                small: this.smallPool
            },
            number: {
                big: this.bigNumber,
                small: this.smallNumber
            },
            total: {
                big: player.bigTotal,
                small: player.smallTotal
            },
            data: data
        };

        this.sendToClient(data.uid, 'betRes', betResData);

        return betResData;
    }

    /**
     *
     *
     * @param {*} player
     * @param {*} data
     * @param {*} err
     * @returns
     * @memberof BigSmallGame
     */
    sendBetErr(player, data, err) {
        this.pendingBets[this.roundId]--;
        let resData;
        if (err.errCode) {
            resData = {
                rid: this.roundId,
                code: "00-006",
                errCode: err.errCode,
                errMsg: err.errMsg,
                pool: {
                    big: this.bigPool,
                    small: this.smallPool
                },
                number: {
                    big: this.bigNumber,
                    small: this.smallNumber
                },
                total: {
                    big: player.bigTotal,
                    small: player.smallTotal
                },
                data: data
            }
        }
        else {
            resData = {
                rid: this.roundId,
                code: "00-005",
                msg: "internal error",
                pool: {
                    big: this.bigPool,
                    small: this.smallPool
                },
                number: {
                    big: this.bigNumber,
                    small: this.smallNumber
                },
                total: {
                    big: player.bigTotal,
                    small: player.smallTotal
                },
                data: data
            }
        }
        this.sendToClient(data.uid, 'betRes', resData);
        return resData;
    }

    /**
     *
     *
     * @param {*} currentTime
     * @returns
     * @memberof BigSmallGame
     */
    sendStateChange(currentTime) {
        let stateChangeData = {
            rid: this.roundId,
            state: this.state,
            currentTime: currentTime,
            nextStateTime: this.nextStateTime,
            pool: {
                big: this.bigPool,
                small: this.smallPool
            },
            number: {
                big: this.bigNumber,
                small: this.smallNumber
            },
            tid: this.tableId
        }
        let sendToRedisData = {
            roundId: this.roundId,
            state: this.state,
            nextStateTime: this.nextStateTime,
            bigPool: this.bigPool,
            smallPool: this.smallPool,
            bigNumber: this.bigNumber,
            smallNumber: this.smallNumber,
            tableId: this.tableId,
            currentTime
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.STATE_CHANGE, data: stateChangeData })
        return { stateChangeData, sendToRedisData };
    }

    /**
     *
     *
     * @param {*} player
     * @param {*} data
     * @returns
     * @memberof BigSmallGame
     */
    sendRebuildInfo(player, data) {
        let rebuildData = this.getRebuildData();
        rebuildData.bet = player.betInfo;
        rebuildData.total = {
            big: player.bigTotal,
            small: player.smallTotal
        }

        this.sendToClient(data.uid, "rebuildInfo", rebuildData);
        return rebuildData
    }

    getRebuildData() {
        return {
            rid: this.roundId,
            state: this.state,
            currentTime: Date.now(),
            nextStateTime: this.nextStateTime,
            pool: {
                big: this.bigPool,
                small: this.smallPool
            },
            number: {
                big: this.bigNumber,
                small: this.smallNumber
            },
            history: this.dieHistory,
            betTime: this.betTime,
            lockTime: this.lockTime,
            animateTime: this.animateTime,
            showdownTime: this.showdownTime,
            chips: this.chips,
            odds: this.odds
        }
    }


}

module.exports = BigSmallGame;
