const PlayerBase = require('../../../utils/base/playerBase');
class BigSmallPlayer extends PlayerBase {
    constructor(para) {
        super(para);
        this.betInfo = [];
        this.smallTotal = 0;
        this.bigTotal = 0;
        this.bigNumber = 0;
        this.smallNumber = 0;
    }

    /**
     *
     *
     * @memberof PrimtivePlayer
     */
    init() {
        this.betInfo = [];
        this.smallTotal = 0;    // 金額 
        this.bigTotal = 0;
        this.bigNumber = 0;     // 次數
        this.smallNumber = 0;
    }

    bet(data) {
        this.serialNumber++;
        this.betInfo.push(data);
        if (data.choice == "big") {
            this.bigNumber += 1;
            this.bigTotal += data.amount;
        }
        else {
            this.smallNumber += 1;
            this.smallTotal += data.amount;
        }
    }

    getBet() {
        return {
            betInfo: JSON.stringify(this.betInfo),
            serialNumber: this.serialNumber,
            bigNumber: this.bigNumber,
            bigTotal: this.bigTotal,
            smallNumber: this.smallNumber,
            smallTotal: this.smallTotal
        }
    }

    getGameBet() {
        return {
            bigNumber: this.bigNumber,
            bigPool: this.bigTotal,
            smallNumber: this.smallNumber,
            smallPool: this.smallTotal
        }
    }
    
    betTransform(data) {
        let container = {
            big: {
                bigNumber: data.amount,
                bigPool: 1,
                smallNumber: 0,
                smallPool: 0
            },
            small: {
                bigNumber: 0,
                bigPool: 0,
                smallNumber: data.amount,
                smallPool: 1
            }
        }

        return container[data.choice];
    }
}
module.exports = BigSmallPlayer;