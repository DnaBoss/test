const PlayerBase = require('../../../utils/base/playerBase');
class HiLoPlayer extends PlayerBase {
    constructor(para) {
        super(para);
        this.hiNumber = 0
        this.hiTotal = 0;
        this.loNumber = 0
        this.loTotal = 0;
        this.elevenNumber = 0
        this.elevenTotal = 0;
    }

    init() {
        this.betInfo = [];
        this.hiNumber = 0
        this.hiTotal = 0;
        this.loNumber = 0
        this.loTotal = 0;
        this.elevenNumber = 0
        this.elevenTotal = 0;
        this.serialNumber = 0;
    }

    bet(data) {
        this.serialNumber++;
        this.betInfo.push(data);
        if (data.choice == 'hi') {
            this.hiNumber += 1;
            this.hiTotal += data.amount;
        }
        if (data.choice == "eleven") {
            this.elevenNumber += 1;
            this.elevenTotal += data.amount;
        }
        if (data.choice == "lo") {
            this.loNumber += 1;
            this.loTotal += data.amount;
        }
    }

    getBet() {
        return {
            betInfo: JSON.stringify(this.betInfo),
            serialNumber: this.serialNumber,
            hiNumber: this.hiNumber,
            hiTotal: this.hiTotal,
            elevenNumber: this.elevenNumber,
            elevenTotal: this.elevenTotal,
            loNumber: this.loNumber,
            loTotal: this.loTotal
        }
    }

    getGameBet() {
        return {
            hiNumber: this.hiNumber,
            hiPool: this.hiTotal,
            elevenNumber: this.elevenNumber,
            elevenPool: this.elevenTotal,
            loNumber: this.loNumber,
            loPool: this.loTotal
        }
    }

    betTransform(data) {
        let container = {
            hi: {
                hiNumber: 1,
                hiPool: data.amount,
                elevenNumber: 0,
                elevenPool: 0,
                loNumber: 0,
                loPool: 0
            },
            lo: {
                hiNumber: 0,
                hiPool: 0,
                elevenNumber: 0,
                elevenPool: 0,
                loNumber: 1,
                loPool: data.amount,
            },
            eleven: {
                hiNumber: 0,
                hiPool: 0,
                elevenNumber: 1,
                elevenPool: data.amount,
                loNumber: 0,
                loPool: 0,
            }
        }

        return container[data.choice];
    }
}
module.exports = HiLoPlayer;