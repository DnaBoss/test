const GameBase = require('../../../utils/base/gameBase.js');
const { genDiceComb, getRandomInt, randomRange } = require('../../../utils/utility');
const consts = require('../../../config/consts.json');

class HiLoGame extends GameBase {
    /**
     *Creates an instance of HiLoGame.
     * @param {*} tableId
     * @param {*} setting
     * @property {Array<number[]>} allElevenDiceComb
     * @property {Array<number[]>} allHiDiceComb
     * @property {Array<number[]>} allLoDiceComb
     * @property {number} hiActualPool
     * @property {number} hiPool
     * @property {number} hiNumber
     * @property {number} loActualPool
     * @property {number} loPool
     * @property {number} loNumber
     * @property {number} elevenActualPool
     * @property {number} elevenPool
     * @property {number} elevenNumber
     * @memberof HiLoGame
     */
    constructor(tableId, setting) {
        super(tableId, setting);
        // 各種骰子的組合
        this.allElevenDiceComb = genDiceComb().allElevenDiceComb;
        this.allHiDiceComb = genDiceComb().allHiDiceComb;
        this.allLoDiceComb = genDiceComb().allLoDiceComb;
        // 造市彩池、真實彩池、造市下注次數、真實下注次數
        this.hiActualPool = 0;
        this.hiPool = 0;
        this.hiNumber = 0;
        this.loActualPool = 0;
        this.loPool = 0;
        this.loNumber = 0;
        this.elevenActualPool = 0;
        this.elevenPool = 0;
        this.elevenNumber = 0;
        this.diceStyle = undefined;
        this.odds = { hi: setting.odds[0], eleven: setting.odds[1], lo: setting.odds[2] };
    }

    /**
     *
     *
     * @memberof HiLoGame
     */
    init() {
        // dummy pool,real pool,dummy bet time ,real bet time
        this.hiActualPool = 0;
        this.hiPool = 0;
        this.hiNumber = 0;
        this.loActualPool = 0;
        this.loPool = 0;
        this.loNumber = 0;
        this.elevenActualPool = 0;
        this.elevenPool = 0;
        this.elevenNumber = 0;
        this.diceStyle = undefined;
        return this;
    }

    redisInit() {
        let initData = {
            hiPool: 0,
            hiNumber: 0,
            loPool: 0,
            loNumber: 0,
            elevenPool: 0,
            elevenNumber: 0
        }
        this.redisDao.clearGameBet(this.tableId, initData);
    }

    /**
     *
     *
     * @param {*} factor
     * @returns
     * @memberof HiLoGame
     */
    async makeFlow(factor) {
        if (this.nextStateTime - Date.now() > this.betTime - this.prebetTime - 3000) {
            return;
        }
        let redisGameBet = await this.redisDao.getGameBet(this.tableId);
        redisGameBet = redisGameBet || {
            hiNumber: this.hiNumber, hiPool: this.hiPool, elevenNumber: this.elevenNumber, elevenPool: this.elevenPool, loNumber: this.loNumber, loPool: this.loPool
        };

        // bigOdd  0:hi,1:eleven,2:lo
        if (factor === 0) {
            let hiFlowNumber = Math.floor(Math.random() * (this.persons[1] - this.persons[0])) + this.persons[0];
            let hiFlowPool = hiFlowNumber * (Math.floor(Math.random() * (this.betAmount[1] - this.betAmount[0])) + this.betAmount[0]);
            this.hiNumber = ~~redisGameBet.hiNumber + hiFlowNumber;
            this.hiPool = ~~redisGameBet.hiPool + (hiFlowPool - hiFlowPool % this.unit);
            return { hiFlowNumber, hiFlowPool, hiNumber: this.hiNumber, hiPool: this.hiPool }
        }
        if (factor === 1) {
            // elevenFlowNumber and elevenFlowPool reduce it from zero to half let it approach to real
            let elevenFlowNumber = Math.floor(this.persons[1] / randomRange(7, 10));
            let elevenFlowPool = elevenFlowNumber * (Math.floor(Math.random() * (this.betAmount[1] - this.betAmount[0]) * 0.5) + this.betAmount[0]);
            this.elevenNumber = ~~redisGameBet.elevenNumber + elevenFlowNumber;
            this.elevenPool = ~~redisGameBet.elevenPool + (elevenFlowPool - elevenFlowPool % this.unit);
            return { elevenFlowNumber, elevenFlowPool, elevenNumber: this.elevenNumber, elevenPool: this.elevenPool }
        }
        if (factor === 2) {
            let loFlowNumber = Math.floor(Math.random() * (this.persons[1] - this.persons[0])) + this.persons[0];
            let loFlowPool = loFlowNumber * (Math.floor(Math.random() * (this.betAmount[1] - this.betAmount[0])) + this.betAmount[0]);
            this.loNumber = ~~redisGameBet.loNumber + loFlowNumber;
            this.loPool = ~~redisGameBet.loPool + (loFlowPool - loFlowPool % this.unit);
            return { loFlowNumber, loFlowPool, loNumber: this.loNumber, loPool: this.loPool }
        }
    }

    /**
     *
     *
     * @param {string} choice
     * @memberof HiLoGame
     */
    checkBetChoice(choice) {
        let data = { code: "02-004", msg: "illegal choice" }
        let ok = true;
        if (choice != "hi" && choice != "lo" && choice != "eleven") {
            ok = false;
        }
        return { ok, data };
    }
    /**
     *
     *
     * @param {Player} player
     * @param {string} choice
     * @param {number} amount
     * @memberof HiLoGame
     */
    async countBet(player, data) {
        const choiceCode = {
            hi: 0,
            eleven: 1,
            lo: 2
        }
        await this.api.getTicket(player.uid, player.actualUcid, this.tableId, this.roundId, data.amount, choiceCode[data.choice], player.ip, player.serialNumber, data.lang);
        this.pendingBets[this.roundId]--;

        if (data.choice == 'hi') {
            this.hiPool += data.amount;
            this.hiActualPool += data.amount;
            this.hiNumber += 1;
        }
        if (data.choice == "eleven") {
            this.elevenPool += data.amount;
            this.elevenActualPool += data.amount;
            this.elevenNumber += 1;
        }
        if (data.choice == "lo") {
            this.loPool += data.amount;
            this.loActualPool += data.amount;
            this.loNumber += 1;
        }
    }

    /**
     *
     *
     * @param {*} winloss
     * @param {*} offset
     * @param {*} actualPools
     * @param {*} odds
     * @returns
     * @memberof HiLoGame
     */
      adjustThreshold(winloss, offset, redisGameBet) {
        let hi = consts.GAME.HI_LO.BASE_RATIO.HI;
        let lo = consts.GAME.HI_LO.BASE_RATIO.LO;
        let eleven = consts.GAME.HI_LO.BASE_RATIO.ELEVEN;

        redisGameBet = redisGameBet || {
            hiPool: this.hiActualPool, loPool: this.loActualPool, elevenPool: this.elevenActualPool
        };
        this.hiActualPool = ~~redisGameBet.hiPool || this.hiActualPool;
        this.loActualPool = ~~redisGameBet.loPool || this.loActualPool;
        this.elevenActualPool = ~~redisGameBet.elevenPool || this.elevenActualPool;
        // decide adjustment or not adjustment
        if (winloss > 0) {
            // probability hi:37.5% eleven:12.5% lo:50%
            // threshold hi:96,lo:128,eleven:32 此數值是依照機率,用256做比例分配
            /*
                判斷該調整哪種玩法的開獎機率,依照offset調整
                調整二種結果的機率,第三種則不動,
                依照賠付的多少來決定,共有六種調整方式,
                開什麼賠最少就加它的機率(threshold 值)
                開什麼賠最多就減它的機率(threshold 值)
            */
            // 計算開出每種結果 會 賺、賠多少
            let hiPayOut = this.hiActualPool * this.odds.hi - this.loActualPool - this.elevenActualPool;
            let loPayOut = this.loActualPool * this.odds.lo - this.hiActualPool - this.elevenActualPool;
            let elevenPayOut = this.elevenActualPool * this.odds.eleven - this.hiActualPool - this.loActualPool;
            let payouts = { hiPayOut, loPayOut, elevenPayOut };
            // 排序 index 0 付給玩家最少 index 2 付玩家最多 ,一樣的話不影響(不需考慮)
            let payoutArr = Object.keys(payouts).sort((a, b) => payouts[a] - payouts[b]);
            // 要賠的錢一樣,則不調整機率
            if (payoutArr[0] == payoutArr[2]) {
                return { hi, lo, eleven };
            }
            // 開 hi 付給玩家最少 且開 lo 會付最多
            if (payoutArr[0] == 'hiPayOut' && payoutArr[2] == 'loPayOut') {
                hi += offset;
                lo -= offset;
            }
            // 開 hi 付給玩家最少 且開 eleven 會付最多
            if (payoutArr[0] == 'hiPayOut' && payoutArr[2] == 'elevenPayOut') {
                hi += offset;
                eleven -= offset;
            }
            // 開 lo 付給玩家最少 且開 hi 會付最多
            if (payoutArr[0] == 'loPayOut' && payoutArr[2] == 'hiPayOut') {
                lo += offset;
                hi -= offset;
            }
            // 開 lo 付給玩家最少 且開 eleven 會付最多
            if (payoutArr[0] == 'loPayOut' && payoutArr[2] == 'elevenPayOut') {
                lo += offset;
                eleven -= offset;
            }
            // 開 eleven 付給玩家最少 且開 hi 會付最多
            if (payoutArr[0] == 'elevenPayOut' && payoutArr[2] == 'hiPayOut') {
                eleven += offset;
                hi -= offset;
            }
            // 開 eleven 付給玩家最少 且開 hi 會付最多
            if (payoutArr[0] == 'elevenPayOut' && payoutArr[2] == 'loPayOut') {
                eleven += offset;
                lo -= offset;
            }
        }
        return { hi, lo, eleven };
    }

    /**
     *
     *
     * @memberof HiLoGame
     */
    async settle() {
        this.diceStyle = Math.random();
        let actualPools = { hi: this.hiActualPool, eleven: this.elevenActualPool, lo: this.loActualPool };
        let odds = { hi: this.odds.hi, lo: this.odds.lo, eleven: this.odds.lo };
        let redisGameBet = await this.redisDao.getGameBet(this.tableId);
        let { hi, lo, eleven } = this.adjustThreshold(this.winloss, this.offset, actualPools, odds, redisGameBet);

        // 對異常的區間值做防呆處理
        // 任何一種結果的機率小於等於 0 或 大於初始值+50或三者相加不等於 100% 則視 offset 的值有誤
        let reset = hi <= 0 ||
            lo <= 0 ||
            eleven <= 0 ||
            hi > consts.GAME.HI_LO.BASE_RATIO.HI + 50 ||
            lo > consts.GAME.HI_LO.BASE_RATIO.LO + 50 ||
            eleven > consts.GAME.HI_LO.BASE_RATIO.ELEVEN + 50 ||
            hi + lo + eleven !== 256
        if (reset) {
            // offset 出錯了,將各自恢復成未調整的數值reset
            hi = consts.GAME.HI_LO.BASE_RATIO.HI;
            lo = consts.GAME.HI_LO.BASE_RATIO.LO;
            eleven = consts.GAME.HI_LO.BASE_RATIO.ELEVEN;
        }
        // logger.debug(`機率區間經過調整?:${this.winloss > 0},hi:${hi},lo:${lo},eleven:${eleven}`);
        let hiRange = [0, hi - 1];
        let elevenRange = [hi, hi + eleven - 1];
        let loRange = [hi + eleven, hi + eleven + lo - 1];
        let randomInt = getRandomInt();
        let outcome;
        if (randomInt >= hiRange[0] && randomInt <= hiRange[1]) {
            outcome = 0; // hi
        }
        if (randomInt >= elevenRange[0] && randomInt <= elevenRange[1]) {
            outcome = 1; // eleven
        }
        if (randomInt >= loRange[0] && randomInt <= loRange[1]) {
            outcome = 2; // lo
        }
        // 隨機取得一種符合結果的骰子組合
        let diceComb = this.getDiceComb(outcome)

        // 用六進位換算後傳給前端
        let num1 = (diceComb[0] - 1) * 1;
        let num2 = (diceComb[1] - 1) * 6;
        let num3 = (diceComb[2] - 1) * 36;

        this.dieNumber = num1 + num2 + num3;

        let resultData = {
            rid: this.roundId,
            dieNumber: this.dieNumber,
            diceStyle: this.diceStyle,
            tid: this.tableId
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.RESULT, data: resultData });
        if (this.dieHistory.length == 10) {
            this.dieHistory.shift();
        }
        this.dieHistory.push({
            rid: this.roundId,
            dieNumber: this.dieNumber
        });
        return {
            winBT: outcome,
            balls: diceComb
        }
    }

    /**
     *
     *
     * @returns {{rid:number,currentTime:number,pool:any,number:any}}
     * @memberof HiLoGame
     */
    sendGameInfo() {
        const gameInfoData = {
            rid: this.roundId,
            currentTime: Date.now(),
            pool: {
                hi: this.hiPool,
                lo: this.loPool,
                eleven: this.elevenPool
            },
            number: {
                hi: this.hiNumber,
                lo: this.loNumber,
                eleven: this.elevenNumber
            },
            tid: this.tableId
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.GAME_INFO, data: gameInfoData });
        return gameInfoData;
    }

    /**
     *
     *
     * @param {Player} player
     * @param {*} data
     * @memberof HiLoGame
     */
    sendBetRes(player, data) {
        let betResData = {
            rid: this.roundId,
            code: "00-000",
            msg: "success",
            pool: {
                hi: this.hiPool,
                eleven: this.elevenPool,
                lo: this.loPool,
            },
            number: {
                hi: this.hiNumber,
                eleven: this.elevenNumber,
                lo: this.loNumber
            },
            total: {
                hi: player.hiTotal,
                eleven: player.elevenTotal,
                lo: player.loTotal
            },
            data: data
        }
        this.sendToClient(data.uid, 'betRes', betResData);
        return betResData;
    }

    /**
     *
     *
     * @param {PlayerBase} player
     * @param {*} data
     * @param {*} err
     * @memberof HiLoGame
     */
    sendBetErr(player, data, err) {
        this.pendingBets[this.roundId]--;
        let betErrData;
        if (err.errCode) {
            betErrData = {
                rid: this.roundId,
                code: "00-006",
                errCode: err.errCode,
                errMsg: err.errMsg,
                pool: {
                    hi: this.hiPool,
                    eleven: this.elevenPool,
                    lo: this.loPool,
                },
                number: {
                    hi: this.hiNumber,
                    eleven: this.elevenNumber,
                    lo: this.loNumber
                },
                total: {
                    hi: player.hiTotal,
                    eleven: player.elevenTotal,
                    lo: player.lootal
                },
                data: data
            }
        }
        else {
            betErrData = {
                rid: this.roundId,
                code: "00-005",
                msg: "internal error",
                pool: {
                    hi: this.hiPool,
                    eleven: this.elevenPool,
                    lo: this.loPool,
                },
                number: {
                    hi: this.hiNumber,
                    eleven: this.elevenNumber,
                    lo: this.loNumber
                },
                total: {
                    hi: player.hiTotal,
                    eleven: player.elevenTotal,
                    lo: player.loTotal
                },
                data: data
            }
        }
        this.sendToClient(data.uid, 'betRes', betErrData);
        return betErrData;
    }

    /**
     *
     *
     * @memberof HiLoGame
     */
    sendStateChange(currentTime) {
        let stateChangeData = {
            rid: this.roundId,
            state: this.state,
            currentTime: currentTime,
            nextStateTime: this.nextStateTime,
            pool: {
                hi: this.hiPool,
                lo: this.loPool,
                eleven: this.elevenPool
            },
            number: {
                hi: this.hiNumber,
                lo: this.loNumber,
                eleven: this.elevenNumber
            },
            tid: this.tableId
        }
        let sendToRedisData = {
            roundId: this.roundId,
            state: this.state,
            nextStateTime: this.nextStateTime,
            hiPool: this.hiPool,
            loPool: this.loPool,
            elevenPool: this.elevenPool,
            hiNumber: this.hiNumber,
            loNumber: this.loNumber,
            elevenNumber: this.elevenNumber,
            tableId: this.tableId,
            currentTime
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.STATE_CHANGE, data: stateChangeData })
        return { stateChangeData, sendToRedisData };
    }


    /**
     *
     *
     * @param {*} player
     * @param {*} data
     * @memberof HiLoGame
     */
    sendRebuildInfo(player, data) {
        let rebuildData = this.getRebuildData();
        rebuildData.bet = player.betInfo;
        rebuildData.total = {
            hi: player.hiTotal,
            lo: player.loTotal,
            eleven: player.elevenTotal
        }
        this.sendToClient(data.uid, "rebuildInfo", rebuildData);
    }

    /**
     *
     *
     * @param {number} outcome
     * @returns {number[]}
     * @memberof HiLoGame
     */
    getDiceComb(outcome) {
        let num;

        // hi 有81種組合
        // elevne 有27種組合
        // lo 有108種組合

        // 0:hi,1:eleven,2:lo
        if (outcome === 0) {
            num = randomRange(0, 80);
            return this.allHiDiceComb[num];
        }
        if (outcome === 1) {
            num = randomRange(0, 26);
            return this.allElevenDiceComb[num];
        }
        if (outcome === 2) {
            num = randomRange(0, 107);
            return this.allLoDiceComb[num];
        }

    }

    getRebuildData() {
        return {
            rid: this.roundId,
            state: this.state,
            currentTime: Date.now(),
            nextStateTime: this.nextStateTime,
            pool: {
                hi: this.hiPool,
                lo: this.loPool,
                eleven: this.elevenPool
            },
            number: {
                hi: this.hiNumber,
                lo: this.loNumber,
                eleven: this.elevenNumber
            },
            history: this.dieHistory,
            betTime: this.betTime,
            lockTime: this.lockTime,
            animateTime: this.animateTime,
            showdownTime: this.showdownTime,
            prebetTime: this.prebetTime,
            chips: this.chips,
            odds: this.odds,
            diceStyle: this.diceStyle
        }
    }


}

module.exports = HiLoGame;
