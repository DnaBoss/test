const GameBase = require('../../../utils/base/gameBase.js');
const { getRandomInt } = require('../../../utils/utility');
const consts = require('../../../config/consts.json');
const oddCodes = [1, 2, 4, 7, 8, 11, 13, 14];
const evenCodes = [0, 3, 5, 6, 9, 10, 12, 15];
class OddEvenGame extends GameBase {
    constructor(tableId, setting) {
        super(tableId, setting);
        this.oddPool = 0;
        this.oddActualPool = 0;
        this.oddNumber = 0;
        this.evenPool = 0;
        this.evenActualPool = 0;
        this.evenNumber = 0;
    }

    init() {
        // init dummy pool,real pool,dummy bet time ,real bet time
        this.oddPool = 0;
        this.oddActualPool = 0;
        this.oddNumber = 0;
        this.evenPool = 0;
        this.evenActualPool = 0;
        this.evenNumber = 0;
    }
    redisInit() {
        let initData = {
            oddPool: 0,
            oddNumber: 0,
            evenPool: 0,
            evenNumber: 0,
        }
        this.redisDao.clearGameBet(this.tableId, initData);
    }
    async  makeFlow(factor) {
        if (this.nextStateTime - Date.now() > this.betTime - this.prebetTime - 3000) {
            return;
        }
        let redisGameBet = await this.redisDao.getGameBet(this.tableId);
        redisGameBet = redisGameBet || {
            oddNumber: this.oddNumber, oddPool: this.oddPool, evenNumber: this.evenNumber, evenPool: this.evenPool
        };

        if (factor == 1) {
            let oddFlowNumber = Math.floor(Math.random() * (this.persons[1] - this.persons[0])) + this.persons[0];
            let oddFlowPool = oddFlowNumber * (Math.floor(Math.random() * (this.betAmount[1] - this.betAmount[0])) + this.betAmount[0]);
            this.oddNumber = ~~redisGameBet.oddNumber + oddFlowNumber;
            this.oddPool = ~~redisGameBet.oddPool + (oddFlowPool - oddFlowPool % this.unit);
        }
        if (factor == 0) {
            let evenFlowNumber = Math.floor(Math.random() * (this.persons[1] - this.persons[0])) + this.persons[0];
            let evenFlowPool = evenFlowNumber * (Math.floor(Math.random() * (this.betAmount[1] - this.betAmount[0])) + this.betAmount[0]);
            this.evenNumber = ~~redisGameBet.evenNumber + evenFlowNumber;
            this.evenPool = ~~redisGameBet.evenPool + (evenFlowPool - evenFlowPool % this.unit);
        }
    }
    /**
     *
     *
     * @param {*} choice
     * @returns
     * @memberof OddEvenGame
     */
    checkBetChoice(choice) {
        let data = { code: "01-004", msg: "illegal choice" }
        let ok = true;
        if (choice != "odd" && choice != "even") {
            ok = false;
        }
        return { ok, data };
    }

    async countBet(player, data) {
        await this.api.getTicket(player.uid, player.actualUcid, this.tableId, this.roundId, data.amount, data.choice == "odd" ? 0 : 1, player.ip, player.serialNumber, data.lang);
        this.pendingBets[this.roundId]--;
        if (data.choice == "odd") {
            this.oddPool += data.amount;
            this.oddActualPool += data.amount;
            this.oddNumber += 1;
        }
        else {
            this.evenPool += data.amount;
            this.evenActualPool += data.amount;
            this.evenNumber += 1;
        }
        return player;
    }

    /**
     *
     *
     * @param {*} winloss
     * @param {*} offset
     * @returns
     * @memberof OddEvenGame
     */
     adjustThreshold(winloss, offset, redisGameBet) {

        redisGameBet = redisGameBet || {
            oddPool: this.oddActualPool, evenPool: this.evenActualPool
        };
        this.oddActualPool = ~~redisGameBet.oddPool|| this.oddActualPool;
        this.evenActualPool = ~~redisGameBet.evenPool || this.evenActualPool;
        let threshold = 127;
        if (winloss > 0) {
            if (this.oddActualPool > this.evenActualPool) {
                threshold = 127 + offset;
            }
            if (this.oddActualPool < this.evenActualPool) {
                threshold = 127 - offset;
            }
        }
        return threshold;
    }

    /**
     *
     *
     * @returns
     * @memberof OddEvenGame
     */
    async  settle() {
        let redisGameBet = await this.redisDao.getGameBet(this.tableId);
        let threshold = this.adjustThreshold(this.winloss, this.offset, redisGameBet);
        let randomInt = getRandomInt();
        let outcome;
        if (randomInt <= threshold) {
            this.dieNumber = evenCodes[Math.floor(Math.random() * 8)];
            outcome = 1; // even
        }
        else {
            this.dieNumber = oddCodes[Math.floor(Math.random() * 8)];
            outcome = 0; // odd
        }
        let resultData = {
            rid: this.roundId,
            dieNumber: this.dieNumber,
            tid: this.tableId
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.RESULT, data: resultData });
        if (this.dieHistory.length == 10) {
            this.dieHistory.shift();
        }
        this.dieHistory.push({
            rid: this.roundId,
            dieNumber: this.dieNumber
        });
        return {
            winBT: outcome,
            balls: [this.dieNumber]
        }
    }

    /**
     * 
     *
     * @returns {{rid:number,currentTime:number,pool:any,number:any}}
     * @memberof OddEvenGame
     */
    sendGameInfo() {
        const gameInfoData = {
            rid: this.roundId,
            currentTime: Date.now(),
            pool: {
                odd: this.oddPool,
                even: this.evenPool
            },
            number: {
                odd: this.oddNumber,
                even: this.evenNumber
            },
            tid: this.tableId
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.GAME_INFO, data: gameInfoData });
        return gameInfoData;
    }

    sendBetRes(player, data) {
        const betResData = {
            rid: this.roundId,
            code: "00-000",
            msg: "success",
            pool: {
                odd: this.oddPool,
                even: this.evenPool
            },
            number: {
                odd: this.oddNumber,
                even: this.evenNumber
            },
            total: {
                odd: player.oddTotal,
                even: player.evenTotal
            },
            data: data
        };
        this.sendToClient(data.uid, 'betRes', betResData);
        return betResData;
    }

    sendBetErr(player, data, err) {
        this.pendingBets[this.roundId]--;
        let resData;
        if (err.errCode) {
            resData = {
                rid: this.roundId,
                code: "00-006",
                errCode: err.errCode,
                errMsg: err.errMsg,
                pool: {
                    odd: this.oddPool,
                    even: this.evenPool
                },
                number: {
                    odd: this.oddNumber,
                    even: this.evenNumber
                },
                total: {
                    odd: player.oddTotal,
                    even: player.evenTotal
                },
                data: data
            }
        }
        else {
            resData = {
                rid: this.roundId,
                code: "00-005",
                msg: "internal error",
                pool: {
                    odd: this.oddPool,
                    even: this.evenPool
                },
                number: {
                    odd: this.oddNumber,
                    even: this.evenNumber
                },
                total: {
                    odd: player.oddTotal,
                    even: player.evenTotal
                },
                data: data
            }
        }
        this.sendToClient(data.uid, 'betRes', resData);
        return resData;
    }

    sendStateChange(currentTime) {
        let stateChangeData = {
            rid: this.roundId,
            state: this.state,
            currentTime: currentTime,
            nextStateTime: this.nextStateTime,
            pool: {
                odd: this.oddPool,
                even: this.evenPool
            },
            number: {
                odd: this.oddNumber,
                even: this.evenNumber
            },
            tid: this.tableId
        }
        let sendToRedisData = {
            roundId: this.roundId,
            state: this.state,
            nextStateTime: this.nextStateTime,
            oddPool: this.oddPool,
            evenPool: this.evenPool,
            oddNumber: this.oddNumber,
            evenNumber: this.evenNumber,
            tableId: this.tableId,
            currentTime
        }
        this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.STATE_CHANGE, data: stateChangeData })

        return { stateChangeData, sendToRedisData };
    }

    sendRebuildInfo(player, data) {

        let rebuildData = this.getRebuildData();
        rebuildData.bet = player.betInfo;
        rebuildData.total = {
            odd: player.oddTotal,
            even: player.evenTotal
        }
        this.sendToClient(data.uid, "rebuildInfo", rebuildData);
        return rebuildData
    }

    getRebuildData() {
        return {
            rid: this.roundId,
            state: this.state,
            currentTime: Date.now(),
            nextStateTime: this.nextStateTime,
            pool: {
                odd: this.oddPool,
                even: this.evenPool
            },
            number: {
                odd: this.oddNumber,
                even: this.evenNumber
            },
            history: this.dieHistory,
            betTime: this.betTime,
            lockTime: this.lockTime,
            animateTime: this.animateTime,
            showdownTime: this.showdownTime,
            prebetTime: this.prebetTime,
            chips: this.chips,
            odds: this.odds
        }
    }


}

module.exports = OddEvenGame;