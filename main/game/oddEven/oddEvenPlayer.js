const PlayerBase = require('../../../utils/base/playerBase');
class OddEvenPlayer extends PlayerBase {
    constructor(para) {
        super(para);
        this.betInfo = [];
        this.evenTotal = 0;
        this.oddTotal = 0;
        this.evenNumber = 0;
        this.oddNumber = 0;
    }

    init() {
        this.betInfo = [];
        this.evenTotal = 0;
        this.oddTotal = 0;
        this.evenNumber = 0;
        this.oddNumber = 0;
    }

    bet(data) {
        this.serialNumber++;
        this.betInfo.push(data);
        if (data.choice == "odd") {
            this.oddNumber += 1;
            this.oddTotal += data.amount;
        }
        else {
            this.evenNumber += 1;
            this.evenTotal += data.amount;
        }
    }

    getBet() {
        return {
            betInfo: JSON.stringify(this.betInfo),
            serialNumber: this.serialNumber,
            oddNumber: this.oddNumber,
            oddTotal: this.oddTotal,
            evenNumber: this.evenNumber,
            evenTotal: this.evenTotal
        }
    }

    getGameBet() {
        return {
            oddNumber: this.oddNumber,
            oddPool: this.oddTotal,
            evenNumber: this.evenNumber,
            evenPool: this.evenTotal
        }
    }

    betTransform(data) {
        let container = {
            odd: {
                oddNumber: 1,
                oddPool: data.amount,
                evenNumber: 0,
                evenPool: 0
            },
            even: {
                oddNumber: 0,
                oddPool: 0,
                evenNumber: 1,
                evenPool: data.amount
            }
        }

        return container[data.choice];
    }
}
module.exports = OddEvenPlayer;