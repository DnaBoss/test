"use strict";

const EventEmitter = require('events');
const { logger } = require('../logger');

const config = require('../../config/load');
const PlayerManager = require('./playerManager');

const consts = require('../../config/consts.json');
const redisDao = require('../../dao/redis').getInstance();

class GameBase extends EventEmitter {
    constructor(tableId, setting) {
        super();
        this.animateTime = setting.consts.TIME.ANIMATE;
        this.api = require('../api').getInstance();
        this.flowSetting = setting.schedule;
        this.betAmount = this.flowSetting[0][0].betAmount;
        this.betTime = parseInt(setting.rules.waitStakeSec) * 1000;
        this.chips = setting.rules.chips;
        this.clubId = parseInt(setting.clubID);
        this.currentTime = 0;
        this.dieHistory = [];
        this.endTime = new Date(setting.endTime);
        this.gameId = parseInt(setting.gameID);

        this.lockTime = setting.consts.TIME.LOCK;
        this.odds = setting.odds;
        this.offset = 0;
        this.pendingBets = {};
        this.persons = this.flowSetting[0][0].persons;
        this.prebetTime = setting.consts.TIME.PRE_BET;
        this.playerManager = new PlayerManager()
        this.pool = {};
        this.queue = [];
        this.resettleRounds = {};
        this.redisDao = redisDao;
        this.roundId;
        this.setEndTimer();
        this.showdownTime = setting.consts.TIME.SHOWDOWN;
        this.tableClosed = false;
        this.tableId = tableId;
        this.unit = parseInt(setting.rules.betFactor) || 1;
    }

    /**
     *
     * @access private
     * @memberof GameBase
     */
    setEndTimer() {
        let leftTime = this.endTime.getTime() - Date.now();

        if (leftTime > consts.COMMON.LEFT_TIME) {
            leftTime = consts.COMMON.LEFT_TIME;
        }
        logger.debug('closeTable timer set.', { iid: this.instanceId, tid: this.tableId, rid: this.roundId, state: this.state });
        this.closeTableHandler = setTimeout(() => {
            let remainTime = this.endTime.getTime() - Date.now();
            if (remainTime > 0) {
                return this.setEndTimer();
            }
            logger.debug('closeTable timer fired.', { iid: this.instanceId, tid: this.tableId, rid: this.roundId, state: this.state });
            this.tableClosed = true;
        }, leftTime);
    }

    /**
     *
     * @access private
     * @param {*} fail
     * @param {*} forced
     * @returns
     * @memberof GameBase
     */
    close(fail, forced) {
        if (forced) {
            this.forcedClosed = 1;
            if (this.state == "STOP") {
                this.emit("closed");
            }
        }
        if (this.tableClosed) {
            return;
        }
        this.tableClosed = true;
        if (fail) {
            this.tableHalt = true;
        }
        clearTimeout(this.closeTableHandler);
    }

    /**
     * control game flow method
     * @access private
     * @param {string} nextState
     * @returns
     * @memberof GameBase
     */
    stateMachine(nextState) {
        this.previousState = this.state;
        this.state = nextState;
        if (this.state == this.previousState) {
            return;
        }
        if (nextState == "BET" && this.tableClosed) {
            if (this.tableHalt) {
                nextState = "STOP";
                this.state = "STOP";
            }
            else {
                nextState = "CLOSED";
                this.state = "CLOSED";
            }
        }
        this.currentTime = Date.now();
        switch (nextState) {
            case "BET": {
                this.pendingBets[this.roundId] = 0;
                this.persons = this.flowSetting[new Date().getDay()][new Date().getHours()].persons;
                this.betAmount = this.flowSetting[new Date().getDay()][new Date().getHours()].betAmount;
                this.startUpdate();
                this.nextStateTime = this.currentTime + this.betTime;
                this.init();
                this.redisInit();
                this.playerManager.initAllPlayer();
                setTimeout(() => {
                    this.stateMachine('LOCK');
                }, this.betTime);
                break;
            }
            case "LOCK": {
                this.stopUpdate();
                this.nextStateTime = this.currentTime + this.lockTime;
                setTimeout(() => {
                    this.stateMachine('ANIMATE');
                }, this.lockTime);
                break;
            }
            case "ANIMATE": {
                let result = this.settle();
                this.nextStateTime = this.currentTime + this.animateTime;
                if (this.pendingBets[this.roundId] > 0) {
                    this.resettleRounds[this.roundId] = result;
                }
                else {
                    delete this.pendingBets[this.roundId];
                }
                this.api.postResult(this.gameId, this.tableId, this.roundId, result).then((res) => {
                    let remainTime = this.nextStateTime - Date.now();
                    this.winloss = res.winloss;
                    if (res.offset) {
                        let offset = parseFloat(res.offset);
                        if (offset <= 50 && offset >= 0) {
                            this.offset = Math.floor(offset / 100 * 256);
                        }
                    }
                    setTimeout(() => {
                        this.stateMachine('SHOWDOWN');
                    }, remainTime)
                }).catch(() => {
                    let msg = `${config.mode}-tid-${this.tableId}-rid-${this.roundId}-settleError`;
                    this.api.notify(msg);
                    this.close(1);
                    this.stateMachine('BET');
                })
                break;
            }
            case "SHOWDOWN": {
                this.nextStateTime = this.currentTime + this.showdownTime;
                this.api.getRoundId(this.tableId, this.roundId).then((rid) => {
                    let remainTime = this.nextStateTime - Date.now();
                    setTimeout(() => {
                        this.roundId = rid;
                        this.stateMachine('BET');
                    }, remainTime)
                }).catch(() => {
                    let msg = config.mode + "-tid-" + this.tableId + "-rid-" + this.roundId + "-newRoundError";
                    this.api.notify(msg);
                    this.close(1);
                    this.stateMachine('BET');
                })
                break;
            }
            case "CLOSED": {
                this.nextStateTime = -1;
                this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.TABLE_CLOSE, data: { tid: this.tableId } })
                this.emit("closed");
                break;
            }
            case "STOP": {
                this.nextStateTime = -1;
                this.redisDao.publishTo(consts.CHANNEL.TABLE, { action: consts.ACTION.TABLE_STOP, data: { tid: this.tableId } })
                if (this.forcedClosed) {
                    this.emit("closed");
                }
                break;
            }
        }

        this.sendStateChange(this.currentTime);

    }


    /**
     *
     *
     * @memberof GameBase
     */
    init() {
        throw ` have yet to implement init , you must implement it`;
    }
    redisInit() {

    }
    /**
     *
     * @access private
     * @param {*} factor
     * @returns
     * @memberof GameBase
     */
    setUpdateTimer(factor) {
        let delay = 100 + 1100 * Math.random();
        if (factor === 0) {
            this.updateHandleEven = this.update(factor, delay);
            return;
        }
        if (factor === 1) {
            this.updateHandleOdd = this.update(factor, delay);
            return;
        }
        if (factor === 2) {
            this.updateHandleLo = this.update(factor, delay);
            return;
        }
    }

    /**
     *
     * @access private
     * @param {*} factor
     * @param {*} delay
     * @returns
     * @memberof GameBase
     */
    update(factor, delay) {
        return setTimeout(() => {
            this.makeFlow(factor);
            this.sendGameInfo();
            this.setUpdateTimer(factor);
        }, delay);
    }

    /**
     *
     * @access private
     * @memberof GameBase
     */
    startUpdate() {
        this.setUpdateTimer(2);
        this.setUpdateTimer(1);
        this.setUpdateTimer(0);
    }

    /**
     *
     * @access private
     * @param {*} factor
     * @memberof GameBase
     */
    makeFlow() {
        throw `have yet to implement makeFlow , you must implement it`
    }

    /**
     *
     * @access private
     * @memberof GameBase
     */
    stopUpdate() {
        clearTimeout(this.updateHandleOdd);
        clearTimeout(this.updateHandleEven);
        clearTimeout(this.updateHandleLo);
        this.updateHandleOdd = null;
        this.updateHandleEven = null;
        this.updateHandleLo = null;
    }


    /**
     *
     * @access public
     * @param {number} gameId
     * @param {*} data
     * @returns
     * @memberof GameBase
     */
    async  join(gameId, data) {
        let player = this.playerManager.getPlyaer(`${data.uid}-${gameId}`);
        if (!player) {
            player = this.playerManager.createPlayer(gameId, data);
            player.getRedisBet(`${data.uid}-${gameId}`);
            player.getRedisLoss(data.uid);
            this.playerManager.addPlayer(`${data.uid}-${gameId}`, player);
        }
        this.sendRebuildInfo(player, data);
        return player;
    }



    /**
     *
     * @access public
     * @param {*} data
     * @returns
     * @memberof GameBase
     */
    async bet(data) {
        let player = this.playerManager.getPlyaer(`${data.uid}-${data.gid}`)
        if (!player) {
            this.sendToClient(data.uid, 'betRes', { code: "00-002", msg: "missing player" });
            this.join(this.gameId, data);
            player = this.playerManager.getPlyaer(`${data.uid}-${data.gid}`);
        }
        if (this.state != 'BET') {
            return this.sendToClient(data.uid, 'betRes', { code: "00-001", msg: "wrong state" });
        }
        data.amount = parseInt(data.amount);
        try {
            this.pendingBets[this.roundId]++;
            await this.countBet(player, data);

            player.bet(data);
            let playerBet = player.getBet();
            let betData = player.betTransform(data);
            // 更新 redis 玩家下注資料
            this.redisDao.setPlayerBet(player.id, playerBet);
            // 累加下注次數及金額到 redis game 
            this.redisDao.incrGameBet(this.tableId, betData);
            this.sendBetRes(player, data);
        }
        catch (err) {
            this.sendBetErr(player, data, err)
        }
        if (this.resettleRounds[this.roundId] && this.pendingBets[this.roundId] == 0) {
            //postResult(this.gameId, this.tableId, currentRid, this.resettleRounds[currentRid]);
            delete this.resettleRounds[this.roundId];
            delete this.pendingBets[this.roundId];
        }
    }

    getRebuildData() {
        throw ` have yet to implement getRebuildData , you must implement it`;
    }

    /**
     *
     *
     * @param {string} choice
     * @memberof GameBase
     */
    checkBetChoice() {
        throw ` have yet to implement checkBetChoice , you must implement it`;
    }
    /**
     *
     * @access public
     * @memberof GameBase
     */
    countBet() {
        throw ` have yet to implement countBet , you must implement it`;
    }

    /**
     *
     *
     * @memberof GameBase
     */
    adjustThreshold() {
        throw `have yet to implement adjustThreshold , you must implement it`;
    }

    /**
     *
     * @access public
     * @memberof GameBase
     */
    settle() {
        throw `have yet to implement settle , you must implement it`;
    }

    /**
     *
     *
     * @param {*} player
     * @param {*} data
     * @memberof GameBase
     */
    sendBetRes() {
        throw `have yet to implement sendBetRes , you must implement it`;
    }

    /**
     *
     *
     * @param {*} player
     * @param {*} data
     * @param {*} err
     * @memberof GameBase
     */
    sendBetErr() {
        throw `have yet to implement sendBetErr , you must implement it`;
    }

    /**
     *
     *
     * @memberof GameBase
     */
    sendGameInfo() {
        throw `have yet to implement sendGameInfo , you must implement it`;
    }

    /**
     *  send init data for player when player login game
     *
     * @memberof GameBase
     */
    sendRebuildInfo() {
        throw `have yet to implement sendRebuildInfo , you must implement it`;
    }

    /**
     *
     *
     * @param {*} currentTime
     * @memberof GameBase
     */
    sendStateChange() {
        throw `have yet to implement sendStateChange , you must implement it`;
    }

    /**
     *
     *
     * @param {*} uid
     * @param {*} event
     * @param {*} data
     * @memberof GameBase
     */
    sendToClient(uid, event, data) {
        data.gid = this.gameId;
        this.emit("sendTo", "uid-" + uid, event, data);
    }

    /**
     *
     *
     * @param {*} event
     * @param {*} data
     * @memberof GameBase
     */
    sendToAll(event, data) {
        data.gid = this.gameId;
        this.emit("sendTo", this.tableId + "-miniGameRoom", event, data);
    }

    /**
     *
     *
     * @param {*} event
     * @param {*} data
     * @memberof GameBase
     */
    sendToFocus(event, data) {
        data.gid = this.gameId;
        this.emit("sendTo", `${this.tableId}-miniGameRoom-update`, event, data);
    }
}

module.exports = GameBase;


