const redisDao = require('../../dao/redis').getInstance();
class PlayerBase {
    constructor(para) {
        this.betInfo = [];
        this.name = para.name;
        this.ip = para.ip;
        this.actualUcid = para.actualUcid;
        this.uid = para.uid;
        this.serialNumber = 0;
        this.id = '';
    }

    /**
     *
     *
     * @memberof PlayerBase
     */
    init() {
        throw `player have yet init `;
    }

    bet() {
        throw `player have yet bet `;
    }

    getBet() {
        throw `player have yet getBet `;
    }

    betTransform() {
        throw `player have yet betTransform `;
    }

    async  getRedisBet(id) {
        let playerBet = await redisDao.getPlayerBet(id);
        if (playerBet) {
            playerBet.betInfo = JSON.parse(playerBet.betInfo);
            this.dump(playerBet);
        }
        return playerBet;
    }

    async  getRedisLoss(id) {
        let playerLoss = await redisDao.getPlayerLoss(id);
        if (playerLoss) {
            this.loss = playerLoss
        }
        return playerLoss;
    }

    async initRedisBet(id) {
        let initData = {}
        Object.getOwnPropertyNames(this).forEach(name => {
            if (this[name] != undefined) {
                initData[name] = this[name]
            }
        });
        initData.betInfo = JSON.stringify(initData.betInfo);
        redisDao.setPlayerBet(id, initData);
    }

    dump(playerBet) {
        Object.keys(playerBet).forEach(key => {
            let hasKey = Object.prototype.hasOwnProperty.call(this, key);
            if (hasKey) {
                this[key] = playerBet[key];
            }
        })
        return this;
    }
}

module.exports = PlayerBase;