
// eslint-disable-next-line no-unused-vars
const PlyaerBase = require('../base/playerBase');
const container = {
    33: require('../../main/game/bigSmall/bigSmallPlayer'),
    34: require('../../main/game/oddEven/oddEvenPlayer'),
    35: require('../../main/game/hiLo/hiLoPlayer')
};
class PlayerManager {
    constructor() {
        this.playerList = {};
    }

    /**
     *
     *
     * @param {number} gid
     * @param {*} data
     * @returns {PlyaerBase}
     * @memberof PlayerManager
     */
    createPlayer(gid, data) {
        let player = new container[gid](data);
        return player;
    }

    /**
     *
     *
     * @memberof PlayerManager
     */
    initAllPlayer() {
        Object.keys(this.playerList).forEach(id => {
            let player = this.getPlyaer(id);
            player.init();
            player.initRedisBet(player.id);
        });
    }

    /**
     *
     *
     * @param {*} id
     * @returns {PlyaerBase}
     * @memberof PlayerManager
     */
    getPlyaer(id) {
        return this.playerList[id];
    }

    /**
     *
     *
     * @param {*} id
     * @param {*} player
     * @memberof PlayerManager
     */
    addPlayer(id, player) {
        player.id = id;
        this.playerList[id] = player;
    }

 
}

module.exports = PlayerManager;