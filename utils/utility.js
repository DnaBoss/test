
"use strict";
const crypto = require("crypto");
/**
 *
 *
 * @returns {{allHiDiceComb:Arrya<number[]>,allElevenDiceComb:Arrya<number[]>,allLoDiceComb:Arrya<number[]>}}
 */
function genDiceComb() {
    let allDice = [];
    for (let i = 1; i <= 6; i++) {
        for (let j = 1; j <= 6; j++) {
            for (let k = 1; k <= 6; k++) {
                allDice.push([i, j, k])
            }
        }
    }
    // hi 有81種組合
    let allHiDiceComb = allDice.filter(arr => arr[0] + arr[1] + arr[2] > 11);
    // elevne 有27種組合
    let allElevenDiceComb = allDice.filter(arr => arr[0] + arr[1] + arr[2] == 11);
    // lo 有108種組合
    let allLoDiceComb = allDice.filter(arr => arr[0] + arr[1] + arr[2] < 11);

    return { allHiDiceComb, allElevenDiceComb, allLoDiceComb }
}
/**
 *
 *
 * @param {*} a number
 * @param {*} b number
 */
function randomRange(a, b) {
    return Math.floor(Math.random() * (b - a)) + a;
}

function getRandomInt() {
    var randomBytes = crypto.randomBytes(1);
    // var randomValue = 0;
    /* Turn the random bytes into an integer, using bitwise operations. */
    return randomBytes[0];
    //  0 <= randomValue <= 255
}
module.exports = {
    genDiceComb,
    getRandomInt,
    randomRange
};