var path = require("path");

const { createLogger, format, transports } = require('winston');
const { combine, label, printf, json } = format;

var file_base = path.resolve(__dirname, '../logs/');
var filename = path.resolve(file_base, "mini-game.log")




let t1 = new transports.File({
    filename: filename,
    handleExceptions: true,
    humanReadableUnhandledException: true,
    //maxsize: 1000000000,
    //maxFiles: 6,
    //tailable: true,
    level: 'debug'
});


var transportList = [t1];

const timestamp = format((info, opts) => {
    let d = new Date();
    
    info.timestamp = 
        num2str(d.getFullYear(), 4) + "-" + num2str(d.getMonth() + 1, 2) + "-" + num2str(d.getDate(), 2)
        + " " + num2str(d.getHours(), 2) + ":" + num2str(d.getMinutes(), 2) + ":" + num2str(d.getSeconds(), 2)
        + "." + num2str(d.getMilliseconds(), 3);
    return info;
});

function num2str(val, n0) {
    var v = val.toString();
    return n0 <= v.length ? v : "0000".substr(0, n0 - v.length) + v;
}

let logger = createLogger({
    format: combine(
        timestamp(),
        json()
    ),
    transports: transportList
});

let switching = false;

function beforeSwitch(){
    if(switching){
        return Promise.reject({message: "switching: " + switching});
    }
    switching = "beforeSwitch";
    return new Promise((resolve, reject)=>{
        let t = new transports.File({
            filename: filename + '.tmp',
            handleExceptions: true,
            humanReadableUnhandledException: true,
            // maxsize: 1000000000,
            // maxFiles: 6,
            // tailable: true,
            level: 'debug'
        });
        let tmpLogger = logger;
        logger = createLogger({
            format: combine(
                timestamp(),
                json()
            ),
            transports: [t]
        });
        setTimeout(()=>{
            tmpLogger.once('finish', function (info) {
                // resolve();
                switching = false;
            });
            tmpLogger.end();
        }, 30000)
        resolve();
    })
}

function afterSwitch(cb = ()=>{}){
    if(switching){
        return Promise.reject({message: "switching: " + switching});
    }
    switching = "afterSwitch";
    return new Promise((resolve, reject)=>{
        let t = new transports.File({
            filename: filename,
            handleExceptions: true,
            humanReadableUnhandledException: true,
            // maxsize: 1000000000,
            // maxFiles: 6,
            // tailable: true,
            level: 'debug'
        });
        let tmpLogger = logger;
        logger = createLogger({
            format: combine(
                timestamp(),
                json()
            ),
            transports: [t]
        });
        setTimeout(()=>{
            tmpLogger.once('finish', function (info) {
                // resolve();
                switching = false;
            });
            tmpLogger.end();
        }, 30000)
        resolve();
    })
}


function loggerWrapperDebug(){
    logger.debug.apply(null, arguments)
}


function loggerWrapperError(){
    logger.error.apply(null, arguments)
}

function loggerWrapperInfo(){
    logger.info.apply(null, arguments)
}

module.exports = {
    logger: {
        debug: loggerWrapperDebug,
        error: loggerWrapperError,
        info: loggerWrapperInfo
    },
    beforeSwitch: beforeSwitch,
    afterSwitch: afterSwitch
};