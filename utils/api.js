
var rp = require('request-promise');
var config = require('../config/load');
const { logger } = require('./logger');
const gameManager = require('./gameManager').getInstance();

class Api {

    /**
     *Creates an instance of Api.
     * @param {*} host
     * @param {*} path
     * @param {*} env
     * @memberof Api
     * @returns {Api}
     */
    constructor() {
        // TODO:正常應該要在這裡吃config,目前先直接使用rpRepty做請求
        this._instance;
    }

    /**
     *
     *
     * @static
     * @returns {Api}
     * @memberof Api
     */
    static getInstance() {
        return this._instance = this._instance || new Api();
    }

    /**
     *
     *
     * @param {*} query
     * @returns {Promise}
     * @memberof Api
     */
    authenticate(query) {
        return new Promise((resolve, reject) => {
            let options = {
                uri: config.api + '/sev/MiniGame/validUserTaixiu.php',
                method: 'POST',
                form: {
                    ucid: encodeURIComponent(query.ucid),
                    _sessid: encodeURIComponent(query._sessid)
                },
                json: true
            };

            rp(options).then((res) => {
                logger.debug('validUserTaixiu', { req: options, res: res });
                if (res.errCode != 0) {
                    reject(res);
                }
                else {
                    resolve(res);
                }
            }).catch((err) => {
                logger.error('validUserTaixiu exception', { req: options, message: err.message, stack: err.stack });
                reject(err);
            });

        })
    }

    /**
     *
     *
     * @returns {Promise}
     * @memberof Api
     */
    getTables() {
        return new Promise((resolve, reject) => {
            if (config.bypass) {
                return resolve(gameManager.getTables());
            }

            var options = {
                uri: config.api + '/sev/MiniGame/getTables.php',
                json: true // Automatically parses the JSON string in the response
            };

            rp(options).then((res) => {
                if (res.errCode != 0) {
                    logger.error('getTables failure: ', { req: options, res: res });
                    return reject(new Error(res.errMsg));
                }
                else {
                    logger.debug('getTables success: ', { req: options, res: res });
                    return resolve(res);
                }
            })
                .catch((err) => {
                    logger.error('getTables exception: ', { req: options, msg: err.message, stack: err.stack });
                    return reject(err);
                });
        })
    }

    /**
     *
     *
     * @param {*} tid
     * @param {*} gid
     * @returns {Promise}
     * @memberof Api
     */
    getTableData(tid, gid) {
        return new Promise((resolve, reject) => {
            if (config.bypass) {
                return resolve(gameManager.getSetting(gid));
            }
            var options = {
                uri: config.api + '/sev/tableData.php',
                qs: {
                    tid: tid
                },
                json: true // Automatically parses the JSON string in the response
            };

            rp(options).then((res) => {
                if (res.errCode != 0) {
                    logger.error('tableData failure: ', { req: options, res: res });
                    return reject(new Error(res.errMsg));
                }
                else {
                    logger.debug('tableData success: ', { req: options, res: res });
                    return resolve(res.table);
                }
            })
                .catch((err) => {
                    logger.error('tableData exception: ', { req: options, msg: err.message, stack: err.stack });
                    return reject(err);
                });
        })
    }


    /**
     *
     *
     * @param {*} tid
     * @param {number} rid
     * @returns {Promise}
     * @memberof Api
     */
    getRoundId(tid, rid) {
        return new Promise((resolve, reject) => {
            if (config.bypass) {
                if (rid == null) {
                    rid = 0;
                }
                else {
                    rid++;
                }
                return resolve(rid);
            }

            var options = {
                uri: config.api + '/sev/newRound.php',
                qs: {
                    tid: tid
                },
                json: true // Automatically parses the JSON string in the response
            };
            rpRetry(options).then((res) => {
                if (res.errCode == 4444) {
                    logger.error('newRound tableClosed', { iid: config.instanceId, tid: tid, rid: rid, req: options, res: res });
                    return reject(res);
                }
                else if (res.errCode != 0) {
                    logger.error('newRound error', { iid: config.instanceId, tid: tid, rid: rid, req: options, res: res });
                    return reject(res);
                }
                else {
                    logger.debug('newRound success', { iid: config.instanceId, tid: tid, rid: rid, req: options, res: res });
                    return resolve(res.roundID);
                }
            })
                .catch((err) => {
                    logger.error('newRound exception', { iid: config.instanceId, tid: tid, rid: rid, req: options, err: err.message, stack: err.stack });
                    return reject(err);
                });
        })
    }

    /**
     *
     *
     * @param {number} gid
     * @param {number} tid
     * @param {number} rid
     * @param {*} result
     * @returns {Promise}
     * @memberof Api
     */
    postResult(gid, tid, rid, result) {
        return new Promise((resolve, reject) => {
            if (config.bypass) {
                return resolve({});
            }
            //logger.debug('settle debug 1')
            let options = {
                uri: config.api + '/sev/MiniGame/settle.php',
                method: 'POST',
                body: {
                    gid: gid,
                    tid: tid,
                    rid: rid,
                    result: result,
                    endTime: Date.now()
                },
                json: true
            };
            //logger.debug('settle debug 1.1')
            rpRetry(options).then((res) => {
                logger.debug('settle', { req: options, res: res });
                if (res.errCode != 0) {
                    reject(res);
                }
                else {
                    resolve(res);
                }
            })
                .catch((err) => {
                    logger.error('settle exception', { req: options, message: err.message, stack: err.stack });
                    reject(err);
                });
            //logger.debug('settle debug 1.2')
        })
    }

    /**
     *
     *
     * @param {*} uid
     * @param {*} ucid
     * @param {*} tid
     * @param {*} rid
     * @param {*} buyIn
     * @param {*} bt
     * @param {*} ip
     * @param {*} serialNum
     * @param {*} lang
     * @returns {Promise}
     * @memberof Api
     */
    getTicket(uid, ucid, tid, rid, buyIn, bt, ip, serialNum, lang) {
        return new Promise((resolve, reject) => {
            if (config.bypass) {
                resolve();
                return;
            }

            let options = {
                uri: config.api + '/sev/MiniGame/bet.php',
                method: 'GET',
                qs: {
                    tid: tid,
                    rid: rid,
                    uid: uid,
                    ucid: ucid,
                    buyIn: buyIn,
                    bt: bt,
                    ip: ip,
                    serialNum: serialNum,
                    lang: lang
                },
                json: true
            };

            rp(options).then((res) => {
                logger.debug('MiniGame bet', { req: options, res: res });
                if (res.errCode != 0) {
                    reject(res);
                }
                else {
                    resolve(res);
                }
            })
                .catch((err) => {
                    logger.error('MiniGame bet exception', { req: options, message: err.message, stack: err.stack });
                    reject(err);
                });
        })
    }

    // 己棄用
    requestGem(number, typeId, iid, tid, rid, uid, cb) {
        //1:rabbit 2:bet time 3:insurance time 4:emoji 5:compulsorySD
        var options = {
            uri: config.api + '/sev/minusGem.php',
            qs: {
                gem: number,
                tid: tid,
                rid: rid,
                uid: uid,
                type: typeId
            },
            json: true // Automatically parses the JSON string in the response
        };

        // if(config.dev){
        //     return cb(null, {gem: 999});
        // }
        rp(options).then((res) => {
            if (res.errCode != 0) {
                logger.error('requestGem error', { iid: iid, tid: tid, rid: rid, req: options, res: res });
                return cb(null, res);
            }
            return cb(null, res);
        })
            .catch((err) => {
                logger.error('requestGem exception', { iid: iid, tid: tid, rid: rid, req: options, err: err.message, stack: err.stack });
                return cb(err, null);
            });
    }

    /**
     *
     *
     * @param {string} msg
     * @returns {Promise}
     * @memberof Api
     */
    notify(msg) {

        return new Promise((resolve, reject) => {
            if (config.bypass) {
                resolve();
                return;
            }
            let options = {
                uri: config.api + '/sev/sendLetstalk.php',
                method: 'GET',
                qs: {
                    msg: msg
                },
                json: true
            };

            rp(options).then((res) => {
                logger.debug('notify', { req: options, res: res });
                if (res.errCode != 0) {
                    reject(res);
                }
                else {
                    resolve(res);
                }
            })
                .catch((err) => {
                    logger.error('notify exception', { req: options, message: err.message, stack: err.stack });
                    reject(err);
                });
        })
    }

    rpRetry(options, retryTime = 3, retryCount = 0) {
        return new Promise(function (resolve, reject) {
            if (retryCount < retryTime) {
                rp(options).then(res => {
                    if (res.errCode != 0) {
                        logger.error('rpRetry error', { req: options, retryTime: retryTime, retryCount: retryCount, res: res });
                        retryCount++;
                        setTimeout(() => {
                            rpRetry(options, retryTime, retryCount).then(res => {
                                resolve(res);
                            }).catch(err => {
                                reject(err);
                            });
                        }, 3000);
                    }
                    else {
                        resolve(res);
                    }
                }).catch((err) => {
                    logger.error('rpRetry exception', { req: options, retryTime: retryTime, retryCount: retryCount, msg: err.message, stack: err.stack });
                    retryCount++;
                    setTimeout(() => {
                        rpRetry(options, retryTime, retryCount).then(res => {
                            resolve(res);
                        }).catch(err => {
                            reject(err);
                        });
                    }, 3000);
                })
            }
            else {
                reject(new Error('exceed retryTime' + retryTime));
            }
        })
    }

}

function rpRetry(options, retryTime = 3, retryCount = 0) {
    return new Promise(function (resolve, reject) {
        if (retryCount < retryTime) {
            rp(options).then(res => {
                if (res.errCode != 0) {
                    logger.error('rpRetry error', { req: options, retryTime: retryTime, retryCount: retryCount, res: res });
                    retryCount++;
                    setTimeout(() => {
                        rpRetry(options, retryTime, retryCount).then(res => {
                            resolve(res);
                        }).catch(err => {
                            reject(err);
                        });
                    }, 3000);
                }
                else {
                    resolve(res);
                }
            }).catch((err) => {
                logger.error('rpRetry exception', { req: options, retryTime: retryTime, retryCount: retryCount, msg: err.message, stack: err.stack });
                retryCount++;
                setTimeout(() => {
                    rpRetry(options, retryTime, retryCount).then(res => {
                        resolve(res);
                    }).catch(err => {
                        reject(err);
                    });
                }, 3000);
            })
        }
        else {
            reject(new Error('exceed retryTime' + retryTime));
        }
    })
}
module.exports = Api;
