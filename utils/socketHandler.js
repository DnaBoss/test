'use strict';
const gameManager = require('../utils/gameManager').getInstance();
const { logger } = require('./logger');
const { sendTo } = require('../web/io');
class SocketHandler {
    /**
     *
     *
     * @static
     * @returns {SocketHandler}
     * @memberof SocketHandler
     */
    static getInstance() {
        return this._instance = this._instance || new SocketHandler();
    }

    onSubscribe(data, tid, gid) {
        let game = gameManager.getGame(tid);
        game.join(gid, data)
    }

    onBet(data) {
        let club = gameManager.getClub(data.ucid);
        if (!club) {
            data.code = "99-003";
            data.msg = "Cannot find this club";
            sendTo("uid-" + data.uid, "betRes", data);
            return;
        }
        const tid = gameManager.getTableId(data.ucid, data.gid);
        if (!tid) {
            data.code = "99-001";
            data.msg = "Cannot find table ID for this club";
            sendTo("uid-" + data.uid, "betRes", data);
            return;
        }
        let game = gameManager.getGame(tid);
        if (!game) {
            data.code = "99-002";
            data.msg = "Creating new table";
            sendTo("uid-" + data.uid, "betRes", data);
            return;
        }
        const checkResult = game.checkBetChoice(data.choice);
        if (!checkResult.ok) {
            logger.debug('checkResult = ', checkResult)
            return sendTo(`uid-${data.uid}`, 'betRes', checkResult.data);
        }
        data.amount = parseInt(data.amount);
        // 不是取消下注,但卻沒帶金額的參數
        if (data.choice != "cancel" && (!Number.isInteger(data.amount) || data.amount <= 0)) {
            return sendTo("uid-" + data.uid, 'betRes', { code: "00-003", msg: "illegal amount" });
        }
        game.bet(data);

    }


    getSocketData(data, socket) {
        data.uid = socket.uid;
        data.ucid = socket.ucid;
        data.actualUcid = socket.actualUcid;
        data.ip = socket.handshake.headers["x-forwarded-for"];
        data.id = socket.id;
        return data;
    }
}

module.exports = SocketHandler;
