

const HiLoGame = require("../main/game/hiLo/hiLoGame");
const BigSmallGame = require("../main/game/bigSmall/bigSmallGame");
const OddEvenGame = require("../main/game/oddEven/oddEvenGame");
const consts = require('../config/consts.json');
const config = require('../config/load');
// eslint-disable-next-line no-unused-vars
const GameBase = require('./base/gameBase');
const { logger } = require('./logger');
const { sendTo } = require('../web/io');

const container = {
    33: BigSmallGame,
    34: OddEvenGame,
    35: HiLoGame
};

class GameManager {

    constructor() {
        this._instance;
        this.tableIdList = {};// {socketId:{gid:tableId}}
        this.gameList = {};// {tableId:table}
    }

    /**
     *
     *
     * @static
     * @param {*} host
     * @param {*} port
     * @param {*} opts
     * @returns {GameManager}
     * @memberof GameManager
     */
    static getInstance() {
        return this._instance = this._instance || new GameManager();
    }

    /**
     *
     *
     * @param {*} tid
     * @param {*} gid
     * @param {*} setting
     * @returns {GameBase}
     * @memberof GameManager
     */
    createGame(tid, gid, setting) {
        let gameName = consts.MAPPING[gid];
        setting.consts = consts.GAME[gameName];
        let game = new container[gid](tid, setting);
        this.gameList[tid] = game;
        return game;
    }

    startGame(tid) {
        let game = this.getGame(tid)
        game.api.getRoundId(tid, this.roundId).then((rid) => {
            game.roundId = rid;
            game.stateMachine('BET');
        }).catch(err => {
            let msg = config.mode + "-tid-" + this.tableId + "-rid-init-newRoundError";
            console.log('errerrerr = ', err)
            game.api.notify(msg);
            game.close(1);
            game.stateMachine('BET');
        })
    }

    gameHook(tid, gid) {
        let game = this.getGame(tid)
        game.on("sendTo", (room, event, data) => {
            sendTo(room, event, data);
        })
        game.on('closed', () => {
            this.closeGame(game.clubId, gid, tid)
        })
    }
    /**
     *
     *
     * @param {*} tid
     * @returns {GameBase}
     * @memberof GameManager
     */
    getGame(tid) {
        return this.gameList[tid];
    }

    /**
     *
     *
     * @param {*} tid
     * @param {*} game
     * @memberof GameManager
     */
    setGame(tid, game) {
        this.gameList[tid] = game;
    }

    /**
     *
     *
     * @param {*} cid
     * @param {*} gid
     * @param {*} tid
     * @memberof GameManager
     */
    closeGame(cid, gid, tid) {
        delete this.gameList[tid]
        if (this.tableIdList[cid] && this.tableIdList[cid][gid] == tid) {
            delete this.tableIdList[cid][gid];
        }
    }

    closeTable(message) {
        logger.debug('closeTable', { msg: message });
        let tableId = parseInt(message);
        let game = this.getGame(tableId);//gameDict[tableId];
        if (game && (typeof game.close == 'function')) {
            logger.debug('closeTable close function invoked', { tid: tableId });
            game.close(null, 1);
        }
    }
    /**
     *
     *
     * @param {*} cid
     * @param {*} gid
     * @returns
     * @memberof GameManager
     */
    getTableId(cid, gid) {
        return this.tableIdList[cid] && this.tableIdList[cid][gid] ? this.tableIdList[cid][gid] : undefined;
    }

    /**
     *
     *
     * @param {*} cid
     * @returns
     * @memberof GameManager
     */
    getClub(cid) {
        return this.tableIdList[cid];
    }

    /**
     *
     *
     * @param {*} cid
     * @param {*} gid
     * @param {*} tid
     * @memberof GameManager
     */
    setTableId(cid, gid, tid) {
        this.tableIdList[cid] = this.tableIdList[cid] || {}
        this.tableIdList[cid][gid] = tid
    }

    /**
     *
     *
     * @param {*} gid
     * @returns
     * @memberof GameManager
     */
    getSetting(gid) {
        let cont = {
            35: {
                gameID: gid,
                endTime: new Date(Date.now() + 3600000),
                rules: {
                    waitStakeSec: 12,
                    chips: consts.GAME.HI_LO.CHIPS
                },
                schedule: consts.COMMON.SCHEDULE,
                odds: consts.GAME.HI_LO.ODDS,
            },
            34: {
                gameID: gid,
                endTime: new Date(Date.now() + 3600000),
                rules: {
                    waitStakeSec: 12,
                    chips: consts.GAME.ODD_EVEN.CHIPS
                },
                schedule: consts.COMMON.SCHEDULE,
                odds: {
                    even: consts.GAME.ODD_EVEN.ODDS.EVEN,
                    odd: consts.GAME.ODD_EVEN.ODDS.ODD
                },
            },
            33: {
                gameID: gid,
                endTime: new Date(Date.now() + 3600000),
                rules: {
                    waitStakeSec: 12,
                    chips: consts.GAME.BIG_SMALL.CHIPS
                },
                schedule: consts.COMMON.SCHEDULE,
                odds: {
                    big: consts.GAME.BIG_SMALL.ODDS.BIG,
                    small: consts.GAME.BIG_SMALL.ODDS.SMALL
                },
            }
        }
        return cont[gid];
    }

    getTables() {
        return {
            errCode: 0,
            tables: [
                {
                    cid: "1",
                    gid: "33",
                    tid: "195832"
                },
                {
                    cid: "1",
                    gid: "34",
                    tid: "196943"
                },
                {
                    cid: "1",
                    gid: "35",
                    tid: "197954"
                }
            ]
        }
    }

}

module.exports = GameManager;
