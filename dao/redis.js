'use strict';
const redis = require("redis");
const { logger } = require('../utils/logger');
let config = require('../config/load');
const instanceId = config.instanceId;
config.redisForDispatcher.retry_strategy = function (options) {
    if (options.error) {
        logger.error('control redis retry', { code: options.error.code, msg: options.error.message, total_retry_time: options.total_retry_time, attempt: options.attempt });
    }
    else {
        logger.error('control redis retry', { total_retry_time: options.total_retry_time, attempt: options.attempt });
    }
    return Math.min(options.attempt * 100, 10000);
}

class RedisDao {
    constructor() {
        this.pub = redis.createClient(config.redisForDispatcher);       // for publish message
        this.sub = redis.createClient(config.redisForDispatcher);       // for listen message
        this.client = redis.createClient(config.redisForDispatcher);    // for access redis db
        this._instance;
    }

    /**
     *
     *
     * @static
     * @returns {RedisDao}
     * @memberof RedisDao
     */
    static getInstance() {
        return this._instance = this._instance || new RedisDao();
    }

    /**
     *
     *
     * @param {*} tid
     * @returns
     * @memberof RedisDao
     */
    getGame(tid) {
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:table:${tid}`;
            this.client.get(key, function (err, res) {
                if (err) {
                    let message = `redis set ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            }.bind(this))
        });
    }

    /**
     *
     *
     * @param {*} tid
     * @param {*} game
     * @returns
     * @memberof RedisDao
     */
    setGame(tid, gameData) {
        return new Promise((resolve) => {
            this.client.set(`env:${config.mode}:mini:table:${tid}`, gameData, function (err, res) {
                if (err) {
                    let message = `redis set mini:table:${tid} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            }.bind(this))
        });
    }

    /**
     *
     *
     * @param {*} tid
     * @param {*} betData
     * @returns
     * @memberof RedisDao
     */
    incrGameBet(tid, betData) {
        let self = this;
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:table:pool:${tid}`;
            Object.keys(betData).forEach((filed, index) => {
                let value = betData[filed];
                self.client.hincrby(key, filed, value, function (err, res) {
                    if (err) {
                        let message = `redis get ${key} err, err.args:${JSON.stringify(err)}`
                        logger.error(message);
                        throw new Error(message);
                    }
                    if (index == Object.keys(betData).length - 1) {
                        resolve(res);
                    }
                })
            });

        });
    }

    /**
     *
     *
     * @param {*} tid
     * @returns
     * @memberof RedisDao
     */
    getGameBet(tid) {
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:table:pool:${tid}`;
            this.client.hgetall(key, function (err, res) {
                if (err) {
                    let message = `redis get ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            })
        });
    }

    /**
     *
     *
     * @param {*} tid
     * @param {*} data
     * @returns
     * @memberof RedisDao
     */
    clearGameBet(tid, data) {
        let self = this;
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:table:pool:${tid}`;
            self.client.hmset(key, data, function (err, res) {
                if (err) {
                    let message = `redis get ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            })

        });
    }

    /**
     *
     *
     * @param {*} id
     * @returns
     * @memberof RedisDao
     */
    getPlayerBet(id) {
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:player:${id}`;
            this.client.hgetall(key, function (err, res) {
                if (err) {
                    let message = `redis get ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            }.bind(this))
        });
    }

    /**
     *
     *
     * @param {*} id
     * @param {*} data
     * @returns
     * @memberof RedisDao
     */
    setPlayerBet(id, data) {
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:player:${id}`;
            this.client.hmset(key, data, function (err, res) {
                if (err) {
                    let message = `redis set ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            }.bind(this))
            // 設置 key 存活時間 五分鐘
            this.client.expire(key, 300, function (err) {
                if (err) {
                    let message = `redis expire ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
            }.bind(this))
        });
    }

    setPlayerLoss(id, data) {
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:player:${id}:loss`;
            this.client.hmset(key, data, function (err, res) {
                if (err) {
                    let message = `redis get ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            })
        });
    }

    getPlayerLoss(id) {
        return new Promise((resolve) => {
            const key = `env:${config.mode}:mini:player:${id}:loss`;
            this.client.hgetall(key, function (err, res) {
                if (err) {
                    let message = `redis get ${key} err, err.args:${JSON.stringify(err)}`
                    logger.error(message);
                    throw new Error(message);
                }
                resolve(res);
            })
        });
    }

    /**
     *
     *
     * @param {string} eventName
     * @param {*} data
     * @memberof RedisDao
     */
    publishTo(eventName, data) {
        eventName = `env:${config.mode}:${eventName}`;
        logger.debug(eventName, { data: data, instanceId: instanceId });
        this.pub.publish(eventName, JSON.stringify(data));
    }

    close() {
        this.pub.quit();
        this.sub.quit();
        this.client.quit();
    }

}

module.exports = RedisDao;
