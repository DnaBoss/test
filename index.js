'use strict';
const { logger } = require('./utils/logger');
// eslint-disable-next-line no-undef
process.on('uncaughtException', function (err) {
    console.log('uncaughtException =', err);
})
const config = require('./config/load');
const consts = require('./config/consts.json');
const { io, server, sendTo } = require('./web/io');
const redisDao = require('./dao/redis').getInstance()
const api = require('./utils/api').getInstance();
const gameManager = require('./utils/gameManager').getInstance();
const socketHandler = require('./utils/socketHandler').getInstance();
// all server getTables at server run immediately
(function () {
    let _channel = `env:${config.mode}:${consts.CHANNEL.TABLE}`;
    redisDao.sub.subscribe(_channel);
    autoGetTable();
    setInterval(autoGetTable, 60000);
})();

if (config.master) {
    redisDao.sub.subscribe('closeTable');
}

redisDao.sub.on("message", function (channel, message) {
    let _channel = `env:${config.mode}:${consts.CHANNEL.TABLE}`;
    if (channel == _channel) {
        message = JSON.parse(message);

        let game = gameManager.getGame(message.data.tid);
        if (!game) {
            logger.error('no game message = ', message)
            return;
        }
        if (message.action == consts.ACTION.STATE_CHANGE) {
            game.sendToAll(consts.ACTION.STATE_CHANGE, message.data);
        }
        if (message.action == consts.ACTION.TABLE_CLOSE) {
            game.sendToAll(consts.ACTION.TABLE_CLOSE, message.data);
        }
        if (message.action == consts.ACTION.RESULT) {
            game.sendToAll(consts.ACTION.RESULT, message.data);
        }
        if (message.action == consts.ACTION.TABLE_STOP) {
            game.sendToAll(consts.ACTION.TABLE_STOP, message.data);
        }
        if (message.action == consts.ACTION.GAME_INFO) {
            game.sendToFocus(consts.ACTION.GAME_INFO, message.data);
        }
    }
    if (channel == consts.CHANNEL.CLOSE_TABLE) {
        gameManager.closeTable(message);
        api.getTables().then((res) => {
            res.tables.forEach(element => {
                gameManager.setTableId(element.cid, element.gid, element.tid);
            })
        }).catch((err) => {
            logger.error('getTables exception', { msg: err.message, stack: err.stack });
        })
    }
});

async function autoGetTable() {
    try {
        let res = await api.getTables();
        res.tables.forEach(getTableData)
    } catch (err) {
        logger.error('getTables exception', { msg: err.message, stack: err.stack });
    }
}

async function getTableData(element) {
    let tableData = await api.getTableData(element.tid, element.gid);
    let game = gameManager.getGame(element.tid);
  
    if (game) {
        return;
    }
    game = gameManager.createGame(element.tid, element.gid, tableData);
    // console.log('gamegamegame = ',game);
    gameManager.setTableId(element.cid, element.gid, element.tid);
    gameManager.setGame(element.tid, game);
    // 將game emit 掛載到 socket emit
    gameManager.gameHook(element.tid, element.gid);
    if (config.master) {
      
        // run state machine
        gameManager.startGame(element.tid);
    }
}

io.use((socket, next) => {
    if (config.bypass) {
        socket.uid = socket.handshake.query.uid;
        socket.ucid = socket.handshake.query.ucid;
        socket.tid = "devTable";
        return next();
    }

    if (socket.handshake.query && socket.handshake.query._sessid && socket.handshake.query.uid && socket.handshake.query.ucid) {
        api.authenticate(socket.handshake.query).then((res) => {
            socket.uid = res.user.id;
            socket.acc = res.user.acc;
            socket.name = res.user.name;
            socket.head = res.user.head;
            socket.actualUcid = res.user.ucid;
            socket.ucid = socket.actualUcid;
            socket.tid = res.tid;
            next();
        }).catch(() => {
            next(new Error('Mini-game authentication exception'));
            socket.disconnect();
        })

    } else {
        next(new Error('Mini-game authentication error'));
        socket.disconnect();
    }
}).on('connect', (socket) => {
    socket.join(`uid-${socket.uid}`, (err) => {

        if (err) {
            logger.error('socket join error', { id: socket.id, uid: socket.uid, message: err.message, stack: err.stack });
            return socket.disconnect();
        }

        socket.on("bet", data => {
            let socketData = socketHandler.getSocketData(data, socket);
            socketHandler.onBet(socketData);
        });

        socket.on("subscribe", data => {
            let socketData = socketHandler.getSocketData(data, socket);
            let noTableGids = [];
            socketData.gids.forEach(gid => {
                let tid = gameManager.getTableId(socketData.ucid, gid);
                if (tid) {
                    socket.join(`${tid}-miniGameRoom`)
                    socketHandler.onSubscribe(socketData, tid, gid);
                }
                else {
                    return noTableGids.push(gid);
                }
                if (noTableGids.length > 0) {
                    return sendTo(`uid-${data.uid}`, "noTable", { gids: noTableGids });
                }
            })
        })

        socket.on("enableUpdate", data => {
            logger.debug('enableUpdate', { msg: data });
            let socketData = socketHandler.getSocketData(data, socket);
            socketData.gids.forEach(gid => {
                let tid = gameManager.getTableId(socketData.ucid, gid);
                if (tid) {
                    socket.join(`${tid}-miniGameRoom-update`, err => {
                        if (err) {
                            logger.error('socket join enableUpdate error', { data: data, message: err.message, stack: err.stack });
                        }
                    })
                }
            })
        })

        socket.on("disableUpdate", data => {
            logger.debug('disableUpdate', { msg: data });
            let socketData = socketHandler.getSocketData(data, socket);
            socketData.gids.forEach(gid => {
                let tid = gameManager.getTableId(socketData.ucid, gid);
                if (tid) {
                    socket.leave(`${tid}-miniGameRoom-update`, err => {
                        if (err) {
                            logger.error('socket leave disableUpdate error', { data: data, message: err.message, stack: err.stack });
                        }
                    })
                }
            })
        })

    });
});

server.listen(config.port, function () {
    logger.debug("app listening on port " + config.port + " mode " + config.mode + ", " + new Date());
    console.log("app listening on port " + config.port + " mode " + config.mode + ", " + new Date());
})


