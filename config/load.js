/* eslint-disable no-undef */
'use strict';

var mode = "dev";
var port = null;
var env = "";
var consolelog = "";
var instanceId = "";
process.argv.forEach((val) => {
    if (/^port(:|=)\d+$/i.test(val)) {
        port = parseInt(val.substr(5));
    }
    if (/^mode(:|=)/i.test(val)) {
        mode = val.substr(5);
    }
    if (/^consolelog(:|=)/i.test(val)) {
        consolelog = val.substr(11).toLowerCase();
    }
    if (/^instanceId(:|=)/i.test(val)) {
        instanceId = val.substr(11);
    }
    // if (/^master(:|=)/i.test(val)) {
    //     master = val.substr(7);
    // }
});




var path = require("path");
var filepath = path.resolve(__dirname, "./config." + mode);


var config = require(filepath);
config.mode = mode;
if (port) {
    config.port = port;
}
if(consolelog==="true" || consolelog==="1"){
    config.consolelog=true;
}
if(consolelog==="false" || consolelog==="0"){
    config.consolelog=false;
}
if(instanceId){
    config.instanceId = instanceId;
}
else if(!config.instanceId){
    config.instanceId = require("os").hostname();
}

if(config.masterList.includes(config.instanceId)){
    config.master = 1;
}

if(config.mode == "dev"){
    config.master = 1;
}

config.env = env;


module.exports = config; 