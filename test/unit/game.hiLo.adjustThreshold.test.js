/* eslint-disable no-undef */

const gameManager = require('../../utils/gameManager').getInstance();
const consts = require('../../config/consts.json')
const expect = require('expect')
const redisDao = require('../../dao/redis').getInstance();
// HiLo adjust threshold
describe('HiLo 調整伐值測試', () => {

    const gid = 35
    const setting = gameManager.getSetting(gid);
    const tid = gameManager.getTables().tables.find(table => table.gid == gid).tid;
    const game = gameManager.createGame(tid, gid, setting);
    const randomOffset = Math.floor(Math.random() * (50 - 1)) + 1;
    let lo = consts.GAME.HI_LO.BASE_RATIO.LO;
    let hi = consts.GAME.HI_LO.BASE_RATIO.HI;
    let eleven = consts.GAME.HI_LO.BASE_RATIO.ELEVEN;
    let redisGameBet = {};
    afterEach(() => { game.close(true, true); redisDao.close(); });
    // beforeEach(async () => { });
    // hi 贏要付最多 lo 贏 付最少
    // adjust threshold,when hiPayout is most loPayout is least
    it('當 hiPayout 最多 loPayout 最少,增加lo伐值,減少hi伐值', () => {
        let winloss = 5000;
        game.hiActualPool = 5000;
        game.loActualPool = 100;
        game.elevenActualPool = 300;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset,redisGameBet);
        expect(adjustedThreshold.lo).toBe(lo + randomOffset);
        expect(adjustedThreshold.hi).toBe(hi - randomOffset);
        expect(adjustedThreshold.eleven).toBe(eleven);
    });
    // hi 贏要付最多 eleven 贏 付最少 ,eleven 機率加高,hi機率最低,lo不變
    // adjust threshold,when hiPayout is most elevenPayout is least
    it('當 hiPayout 最多 elevenPayout 最少,增加eleven伐值,減少hi伐值', () => {
        let winloss = 5000;
        game.hiActualPool = 5000;
        game.loActualPool = 3000;
        game.elevenActualPool = 10;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset,redisGameBet);
        expect(adjustedThreshold.eleven).toBe(eleven + randomOffset);
        expect(adjustedThreshold.hi).toBe(hi - randomOffset);
        expect(adjustedThreshold.lo).toBe(lo);
    });
    // lo 贏要付最多 hi 贏 付最少 ,hi 機率加高,lo機率最低,eleven不變
    // adjust threshold,when loPayout is most hiPayout is least
    it('當 loPayout 最多 hiPayout 最少,增加hi伐值,減少lo伐值', () => {
        let winloss = 5000;
        game.hiActualPool = 100;
        game.loActualPool = 8000;
        game.elevenActualPool = 100;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset,redisGameBet);
        expect(adjustedThreshold.lo).toBe(lo - randomOffset);
        expect(adjustedThreshold.hi).toBe(hi + randomOffset);
        expect(adjustedThreshold.eleven).toBe(eleven);
    });
    // lo 贏要付最多 eleven 贏 付最少 ,eleven 機率加高,lo機率減低,hi不變
    // adjust threshold,when loPayout is most elevenPayout is least
    it('當 loPayout 最多 elevenPayout 最少,增加eleven伐值,減少lo伐值', () => {
        let winloss = 5000;
        game.hiActualPool = 2000;
        game.loActualPool = 8000;
        game.elevenActualPool = 100;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset,redisGameBet);
        expect(adjustedThreshold.lo).toBe(lo - randomOffset);
        expect(adjustedThreshold.eleven).toBe(eleven + randomOffset);
        expect(adjustedThreshold.hi).toBe(hi);
    });
    // eleven 贏要付最多 hi 贏 付最少 ,hi 機率加高,eleven機率減低,lo不變
    // adjust threshold,when elevenPayout is most hiPayout is least
    it('當 elevenPayout 最多 hiPayout 最少,增加hi伐值,減少eleven伐值', () => {
        let winloss = 5000;
        game.hiActualPool = 2000;
        game.loActualPool = 8000;
        game.elevenActualPool = 10000;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset,redisGameBet);
        expect(adjustedThreshold.eleven).toBe(eleven - randomOffset);
        expect(adjustedThreshold.hi).toBe(hi + randomOffset);
        expect(adjustedThreshold.lo).toBe(lo);
    });
    // eleven 贏要付最多 lo 贏 付最少 ,lo 機率加高,eleven機率減低,hi不變
    // adjust threshold,when elevenPayout is most loPayout is least
    it('當 elevenPayout 最多 loPayout 最少,增加lo伐值,減少eleven伐值', () => {
        let winloss = 5000;
        game.hiActualPool = 8000;
        game.loActualPool = 2000;
        game.elevenActualPool = 10000;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset,redisGameBet);
        expect(adjustedThreshold.eleven).toBe(eleven - randomOffset);
        expect(adjustedThreshold.lo).toBe(lo + randomOffset);
        expect(adjustedThreshold.hi).toBe(hi);
    });
    // winloo 小於零,不調整機率
    it('當 winloss 小於等於 0,伐值不調整', () => {
        let winloss = 0;
        game.hiActualPool = 5000;
        game.loActualPool = 1077770;
        game.elevenActualPool = 90909;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset,redisGameBet);
        expect(adjustedThreshold.lo).toBe(lo);
        expect(adjustedThreshold.hi).toBe(hi);
        expect(adjustedThreshold.eleven).toBe(eleven);
    });

});
