/* eslint-disable no-undef */

const gameManager = require('../../utils/gameManager').getInstance();
const expect = require('expect')
const redisDao = require('../../dao/redis').getInstance();

// big small adjust threshold
describe('big small 調整伐值測試', () => {

    const gid = 33
    const setting = gameManager.getSetting(gid);
    const tid = gameManager.getTables().tables.find(table => table.gid == gid).tid;

    let game = gameManager.createGame(tid, gid, setting);
    let redisGameBet = {};
    afterEach(() => { game.close(true, true); redisDao.close(); });
    // beforeEach(async () => { });
    // big small adjus threshold by offset,if winloss > 0
    it('當 bigPool 大於 smallPool 伐值會加上 offset 的值', async () => {
        let winloss = 5000;
        let threshold = 127;
        let randomOffset = Math.floor(Math.random() * (50 - 1)) + 1;
        game.bigActualPool = 5000;
        game.smallActualPool = 4900;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset, redisGameBet);
        expect(adjustedThreshold).toBe(threshold + randomOffset);
    });

    it('當 bigPool 小於 smallPool 伐值會減去 offset 的值', async () => {
        let winloss = 5000;
        let threshold = 127;
        let randomOffset = Math.floor(Math.random() * (50 - 1)) + 1;
        game.bigActualPool = 4700;
        game.smallActualPool = 4900;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset, redisGameBet);
        expect(adjustedThreshold).toBe(threshold - randomOffset);
    });

    it('當 winloss 小於等於 0,伐值不調整', async () => {
        let winloss = 5000;
        let offset = 0;
        let threshold = 127;
        let adjustedThreshold = game.adjustThreshold(winloss, offset, redisGameBet);
        expect(adjustedThreshold).toBe(threshold);
    });

});
