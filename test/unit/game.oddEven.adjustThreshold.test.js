/* eslint-disable no-undef */

const gameManager = require('../../utils/gameManager').getInstance();
const expect = require('expect');
const redisDao = require('../../dao/redis').getInstance();
describe('odd even 調整伐值測試', () => {
    const gid = 34
    const setting = gameManager.getSetting(gid);
    const tid = gameManager.getTables().tables.find(table => table.gid == gid).tid;
    let game = gameManager.createGame(tid, gid, setting);
    let redisGameBet = {};
    afterEach(() => { game.close(true, true); redisDao.close(); });
    // beforeEach(async () => { });

    // odd even adjus threshold by offset,if winloss > 0 and oddPool > evenPool
    it('當 oddPoos 大於 evenPool ,伐值會加上 offset 的值', () => {
        let winloss = 5000;
        let threshold = 127;
        let randomOffset = Math.floor(Math.random() * (50 - 1)) + 1;
        game.oddActualPool = 5000;
        game.evenActualPool = 4900;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset, redisGameBet);
        expect(adjustedThreshold).toBe(threshold + randomOffset);
    });

    // odd even adjus threshold by offset,if winloss > 0 and oddPoos < evenPool
    it('當 oddPoos 小於 evenPool ,伐值會減去 offset 的值', () => {
        let winloss = 5000;
        let threshold = 127;
        let randomOffset = Math.floor(Math.random() * (50 - 1)) + 1;
        game.oddActualPool = 4700;
        game.evenActualPool = 4900;
        let adjustedThreshold = game.adjustThreshold(winloss, randomOffset, redisGameBet);
        expect(adjustedThreshold).toBe(threshold - randomOffset);
    });

    // odd even no adjus threshold,if winloss <= 0
    it('當 winloss 小於等於 0,伐值不調整', () => {
        let winloss = 5000;
        let offset = 0;
        let threshold = 127;
        let adjustedThreshold = game.adjustThreshold(winloss, offset, redisGameBet);
        expect(adjustedThreshold).toBe(threshold);
    });
});
