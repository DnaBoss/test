var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var config = require("../config/load");
var helmet = require('helmet');
const {logger, beforeSwitch, afterSwitch} = require('../utils/logger');

app.use(helmet());

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use('/', express.static('public'));

// No need to check session authenticated or not.

let hostname = require("os").hostname();

app.use('/health', (req, res)=>{
    return res.json({errCode: 0, time: Date.now(), port: config.port, id: config.instanceId, host: hostname}).end();
});

app.use('/beforeSwitch', (req, res)=>{
    console.log("beforeSwitch req at " + new Date());
    let startTime = Date.now();
    logger.debug("beforeSwitch before");

    beforeSwitch().then(()=>{
        console.debug("beforeSwitch after, " + Math.ceil((Date.now() - startTime) / 1000) + "s");
        logger.debug("beforeSwitch after");
        return res.json({success: 1});
    })
    .catch(err=>{
        logger.error('beforeSwitch', {msg: err.message, stack: err.stack});
        console.log("beforeSwitch respond error, " + Math.ceil((Date.now() - startTime) / 1000) + "s, msg: " + err.message);
        return res.json({success: 0, msg: err.message});
    })

});

app.use('/afterSwitch', (req, res)=>{
    console.log("afterSwitch req at " + new Date());
    let startTime = Date.now();
    logger.debug("afterSwitch before");

    afterSwitch().then(()=>{
        console.debug("afterSwitch after, " + Math.ceil((Date.now() - startTime) / 1000) + "s");
        logger.debug("afterSwitch after");

        return res.json({success: 1});
    })
    .catch(err=>{
        logger.error('afterSwitch', {msg: err.message, stack: err.stack});
        console.log("afterSwitch respond error, " + Math.ceil((Date.now() - startTime) / 1000) + "s, msg: " + err.message);
        return res.json({success: 0, msg: err.message});
    })
});



module.exports = app;
