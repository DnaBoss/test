var config = require("../config/load");
var app = require("./app");
var server;

if(config.env=='staging'){
    var fs = require("fs");

    var sslOptions = {
        key: fs.readFileSync('/etc/letsencrypt/live/tiger88.date/privkey.pem'),
        cert: fs.readFileSync('/etc/letsencrypt/live/tiger88.date/cert.pem')
    };

    server = require("https").createServer(sslOptions, app);
}
else{
    server = require("http").createServer(app);
}


module.exports = server;