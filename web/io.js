'use strict';
var config = require("../config/load");
const {logger} = require('../utils/logger');
// const redis = require("redis");

let instanceId = config.instanceId;

var server;
// if(config.dev || config.site == 'mb'){
    server = require('./server');
// }
// else{
//     server = require('http').createServer();
// }

var io = require('socket.io')(server, {
    path: '/mini',
    pingInterval: 10000,
    pingTimeout: 5000
});


// config.redisAdapter.retry_strategy = function (options) {
//     if(options.error){
//         logger.error('adapter redis retry', {code: options.error.code, msg: options.error.message, total_retry_time: options.total_retry_time, attempt: options.attempt});
//     }
//     else{
//         logger.error('adapter redis retry', {total_retry_time: options.total_retry_time, attempt: options.attempt});
//     }

//     return Math.min(options.attempt * 100, 10000);
// }

// var pub = redis.createClient(config.redisAdapter);

// var sub = redis.createClient(config.redisAdapter);


// var redisAdapter = require('socket.io-redis');
// io.adapter(redisAdapter({pubClient: pub, subClient: sub}));

var sendTo = function(){
    let args = [];
    for (let i = 1; i < arguments.length; i++){
        args.push(arguments[i]);
    }
    let sender = io.to(arguments[0]); 
    sender.emit.apply(sender, args);
    logger.debug(args[0], {
        eventData: args.slice(1), to: arguments[0], instanceId: instanceId
    });
}


module.exports = {
    io: io,
    server: server,
    sendTo: sendTo
};